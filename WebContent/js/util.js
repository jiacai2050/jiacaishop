String.prototype.trim=function(){
	return this.replace(/(^\s*)|(\s*$)/g, "");
}
 
function isdigit(input) {
	return parseFloat(input)==input;
}
function isinteger(input) {
	return parseInt(input)==input;
}
function pnumblur(input, idx, inOrOut) {
	if(!isinteger(input.value)&&input.value!="") {
			alert("必须为整数");
			input.value="";
			input.focus();
	} else {
		
		switch(inOrOut) {
		case 1://入库
			
			break;
		case 2://出库
			var remain = $("#remain"+idx).val();
			if(parseInt(remain)<parseInt(input.value)) {
				alert("本产品库存为"+remain+"，您输入了" + input.value);
				input.value="";
				input.focus();
			}
			break;	
		}
	}
	var unitprice = $("#unitprice"+idx).val();
	if(isdigit(unitprice) && isdigit(input.value)) {
		var tmp = unitprice * input.value;
		$("#sumprice"+idx)[0].val(Math.round(tmp*100)/100);
	}
}
function punitblur(input,idx) {
	if(!isdigit(input.value)&&input.value!="") {
				alert("必须为正数");
				input.value="";
				input.focus();
	}
	var number = $("#number"+idx).val();
	if(isdigit(number) && isdigit(input.value)) {
		var tmp = number * input.value;
		$("#sumprice"+idx).val(Math.round(tmp*100)/100);
	}
}

function addsum() {
	var money = $("input[name^=sumprice]");
	var sum = 0;
	for(var i=1;i<=money.length;i++) {
		var v = $("input[name=sumprice"+i+"]")[0].value;
		if(isdigit(v)) {
			sum += parseFloat(v);
		}
	}
	$("#sum").val(Math.round(sum*100)/100);
}

function addProductTd(i,inOrOut) {//inOrOut=1,入库    =2 出库
	var arr = new Array();
	arr.push("<tr><td>");
	arr.push(i);
	arr.push("</td>");
	arr.push("<td><input type='text' readonly='readonly' onfocus='pickProduct(this,"+inOrOut+","+i+")' name='product");
	arr.push(i);
	arr.push("'/><input type='hidden' name='pid");
	arr.push(i);
	arr.push("'/><input type='hidden' id='remain");
	arr.push(i);
	arr.push("'/></td>");
	arr.push("<td><input type='text' readonly='readonly' name='specification");
	arr.push(i);
	arr.push("'/></td>");
	arr.push("<td><input type='text' onblur='pnumblur(this,"+i+","+inOrOut+")' id='number"+i+"' name='number");
	arr.push(i);
	arr.push("'/></td>");
	arr.push("<td><input type='text' onblur='punitblur(this,"+i+")' id='unitprice"+i+"' name='unitprice");
	arr.push(i);
	arr.push("'/></td>");
	arr.push("<td><input type='text' readonly='readonly' id='sumprice"+i+"' name='sumprice");
	arr.push(i);
	arr.push("'/></td></tr>");
	return arr;
}
function checkProductNumber() {
	addsum();
	var spec = $("input[name^='number']");
	var index = 0;
	for (var i = 0; i < spec.length; i++) {
		if (isinteger(spec[i].value)) { // 查看这次商品一个做多少份货物
			index++;
		}
	}
	$("#pnumber").val(index);
}
function initDateTime() {
	var now = new Date();
	var year = now.getFullYear();
	var month = now.getMonth();
	var date = now.getDate();
	$("#date").val(year + "-" + (month + 1) + "-" + date);
}
function pickProduct(obj,inOrOut,idx) {//inOrOut=1,入库    =2 出库
	$.dialog({
		id : 'pickFrame',
		title : '选择商品',
		content : 'url:product/search.jsp',
		lock : true,
		okVal : '确认',
		cancelVal : '关闭',
		ok : function() {
			var picked = $('input[type=radio]:checked',
					parent.frames['pickFrame'].document);
			if (picked.length == 1) {
				var num = $(picked[0]).parent().prev();
				var spec = num.prev();
				var pname = spec.prev();

				$(obj).next().val(picked[0].value); //保存产品id
				$(obj).next().next().val(num.html());//保存产品库存
				$(obj).val(pname.html());
				$(obj).parent().next().children()[0].value = spec.html();
				
				$.ajax({
					url : "PriceAjax",
					type : "get",
					dataType : "text",
					data : {
						"pid" :picked[0].value,
						"gid" :$("#gid").val()
					},
					success : function(res) {
						if(inOrOut == 2) {
							var result = parseInt(res);
							if (result>0) {
								$("#unitprice"+idx).val(result);
							}	
						} else {
							$("#unitprice"+idx).val(0);
						}
					}
				});//ajax
			} else
				alert("您没有选择任何产品");

		},
		width : 788,
		height : 504,
		cancel : function() {
			alert("您没有选择任何产品");
		}
	});
}
function pickGuest(obj) {
	$.dialog({
		id : 'pickFrame',
		title : '选择客户',
		content : 'url:guest/search.jsp',
		lock : true,
		okVal : '确认',
		cancelVal : '关闭',
		ok : function() {
			var picked = $('input[type=radio]:checked',
					parent.frames['pickFrame'].document);
			if (picked.length == 1) {
				var gaddr = $(picked[0]).parent().prev();
				var gtel = gaddr.prev();
				var gname = gtel.prev();

				$("#gaddr").val(gaddr.html());
				$("#gtel").val(gtel.html());
				$("#gname").val(gname.html());
				$("#gid").val(picked[0].value);
			} else
				alert("您没有选择任何客户");

		},
		width : 788,
		height : 504,
		cancel : function() {
			alert("您没有选择任何产客户");
		}
	});
}