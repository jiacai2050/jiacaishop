<%@ page language="java" pageEncoding="UTF-8"%>
<%@include file="../checklogin.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<title>入库</title>
<base href="<%=basePath%>" />
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">

<script type="text/javascript" src="js/jquery-1.7.2.js"></script>
<script type="text/javascript" src="js/calendar.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.22.custom.min.js"></script>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/tb.js"></script>
<script type="text/javascript" src="js/ChurAlert.min.js?skin=blue"></script>
<script type="text/javascript" src="js/chur-alert.1.0.js"></script>
<link rel="stylesheet" type="text/css" href="style/admin-all.css" />
<link rel="stylesheet" type="text/css" href="style/base.css" />
<link rel="stylesheet" type="text/css"
	href="style/ui-lightness/jquery-ui-1.8.22.custom.css" />
<link rel="stylesheet" type="text/css" href="style/formui.css" />


<script type="text/javascript">
	$(function() {
		initDateTime();
		var times = 1;
		$("#more").click(function() {
			for (var i = times; i < times + 5; i++) {
				$("#prod").append(addProductTd(i, 1).join(""));//1与Commodity表的kind对应，表示入库
			}
			times += 5;
		});
		$("#more").click();
		$("#more").click();
		$("#sub").click(checkProductNumber);
		$("#add").click(addsum);
	});
</script>
</head>

<body>
	<div class="alert alert-info">
		当前位置<b class="tip"></b>库存管理<b class="tip"></b>入库添加
	</div>
	<form action="AddImport" method="get">
		<table class="tbform">
			<tbody>
				<tr>
					<td class="tdl">日期</td>
					<td class="detail"><input type="text" name="date" id="date"
						onclick="SelectDate(this,'yyyy-MM-dd')" readonly="readonly" /></td>

					<td class="tdl">供应商</td>
					<td class="detail"><input type="text" name="supplier" /></td>
				</tr>
			</tbody>
		</table>

		<table class="tb" id="prod">
			<tr>
				<th>序号</th>
				<th>产品名称</th>
				<th>规格</th>
				<th>数量</th>
				<th>单价</th>
				<th>总价</th>
			</tr>
			<!-- placeholder -->
		</table>
		<br />
		<table align="center">
			<tr>
				<td><input type="text" style="width: 150px" name="sum" id="sum"
					readonly="readonly" /> <input class="btn btn-primary add"
					type="button" id="add" value="求总和" /> <input
					class="btn btn-primary add" type="button" id="more" value="新增" />
				</td>
			</tr>
			<tr>
				<td><input
					style="width: 150px; height: 75px; color: red; font-size: 25pt;"
					type="submit" id="sub" value="提交订单" /></td>
			</tr>
		</table>
		<input type="hidden" id="pnumber" name="pnumber" />
	</form>
</body>
</html>
