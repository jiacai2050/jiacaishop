<%@ page language="java"
	import="com.jiacai.bean.*,com.jiacai.dao.*,com.jiacai.util.*,java.util.*"
	pageEncoding="UTF-8"%>
<%@include file="../checklogin.jsp"%>
<%
	String id = request.getParameter("id");
ImportDAO dao = new ImportDAO();
Import im = dao.select(StringUtil.str2int(id));
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>详细信息</title>
<base href="<%=basePath%>" />
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<script type="text/javascript" src="js/jquery-1.7.2.js"></script>
<script type="text/javascript" src="js/calendar.js"></script>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.22.custom.min.js"></script>
<script type="text/javascript" src="js/tb.js"></script>
<link rel="stylesheet" type="text/css" href="style/admin-all.css" />
<link rel="stylesheet" type="text/css" href="style/base.css" />
<link rel="stylesheet" type="text/css"
	href="style/ui-lightness/jquery-ui-1.8.22.custom.css" />
<link rel="stylesheet" type="text/css" href="style/formui.css" />
<script type="text/javascript">
	function del() {
		if (confirm("确认删除入库单？")) {
			$.ajax({
				url : "DeleteAjax",
				type : "get",
				dataType : "text",
				data : {
					"id" :
<%=id%>
	,
					"from" : "import"
				},
				success : function(res) {
					if ("1" == res) {
						alert("删除成功！");
						window.location = "import/add.jsp"
					} else {
						alert("删除失败！");
					}
				}
			});//ajax
		}
	}
</script>
 
</head>

<body>
	<div class="alert alert-info">
		当前位置<b class="tip"></b>库存管理<b class="tip"></b>库存详情
	</div>
	<table class="tbform">
		<tr>
			<td class="tdl">日期</td>
			<td class="detail"><%=StringUtil.date2str(im.getDate())%></td>
			<td class="tdl">供应商</td>
			<td class="detail"><%=im.getSupplier()%></td>
	</table>
	<table class="tb">
		<tr>
			<td align="center">产品名称</td>
			<td align="center">规格</td>
			<td align="center">数量(台)</td>
			<td align="center">单价(元)</td>
			<td align="center">总价(元)</td>
		</tr>
		<%
			CommodityDAO cdao = new CommodityDAO();
			ProductDAO pdao = new ProductDAO();
			List<Commodity> commodities = cdao
					.select(StringUtil.str2int(id), 1);
			Product product = null;
			for (Commodity commodity : commodities) {
				int pid = commodity.getPid();
				product = pdao.select(pid);
				out.println("<tr><td align='center'>" + product.getName()
						+ "</td>");
				out.println("<td align='center'>" + product.getSpecification()
						+ "</td>");
				out.println("<td align='center'>" + commodity.getNumber()
						+ "</td>");
				out.println("<td align='center'>" + commodity.getUnitprice()
						+ "</td>");
				out.println("<td align='center'>" + commodity.getSumprice()
						+ "</td></tr>");

			}
		%>
		<tr align='right'>
			<td align="right" colspan='4'><font style='font-size: 24pt;'>合计</font></td>
			<td align='center'><font style='font-size: 24pt;'><%=im.getSumprice()%></font></td>
		</tr>
	</table>
	<table align='center'>
		<tr>
			<td><a class="btn btn-primary add" href="import/edit.jsp?id=<%=id%>">修改</a>
			----
		<input class="btn btn-primary add" type="button" onclick="del()" value="删除"></td>
		</tr>
	</table>
</body>
</html>
