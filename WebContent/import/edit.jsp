<%@ page language="java"
	import="java.util.*,com.jiacai.bean.*,com.jiacai.dao.*,com.jiacai.util.*"
	pageEncoding="UTF-8"%>
<%@include file="../checklogin.jsp"%>
<%
	int id = StringUtil.str2int(request.getParameter("id"));
	ImportDAO dao = new ImportDAO();
	Import im = dao.select(id);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>修改入库单信息</title>
<base href="<%=basePath%>" />
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">

<script type="text/javascript" src="js/jquery-1.7.2.js"></script>
<script type="text/javascript" src="js/calendar.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.22.custom.min.js"></script>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/tb.js"></script>
<script type="text/javascript" src="js/ChurAlert.min.js?skin=blue"></script>
<script type="text/javascript" src="js/chur-alert.1.0.js"></script>
<link rel="stylesheet" type="text/css" href="style/admin-all.css" />
<link rel="stylesheet" type="text/css" href="style/base.css" />
<link rel="stylesheet" type="text/css"
	href="style/ui-lightness/jquery-ui-1.8.22.custom.css" />
<link rel="stylesheet" type="text/css" href="style/formui.css" />

<script type="text/javascript">
	function delproduct(self, productId) {
		//if(confirm("确认要删除本件商品吗？")) {
		if (productId != "-1") {
			oldids = $("#delids").val();
			if (oldids == null || oldids == "")
				$("#delids").val(productId);
			else
				$("#delids").val(oldids + "," + productId);
		}
		colname = [ "", "name", "specification", "number", "unitprice",
				"sumprice"];
		itemno = parseInt(self.parentNode.parentNode.firstChild.innerHTML);
		//alert
		var nextall = $(self).parent().parent().nextAll();
		$.each(nextall, function(idx, obj) {
			$.each($(obj).children(),
					function(tdIdx, td) {
						var newidx = itemno + idx;
						if (tdIdx == 1) {
							td.firstChild.setAttribute("name", colname[tdIdx] + newidx);
							$(td).children("input:nth-child(2)").attr("name", "pid"+newidx);
							$(td).children("input:nth-child(3)").attr("name", "remain"+newidx);
							$(td).children("input:nth-child(4)").attr("name", "cid"+newidx);
						} else if (tdIdx == 0)
							$(td).html(newidx);
						else
							td.firstChild.setAttribute("name", colname[tdIdx]
									+ newidx);
					});
		});
		$("#pnumber").val($("#pnumber").val() - 1);
		$(self).parent().parent().remove();
	}
	$(function() {
		$("#more")
				.click(
						function() {
							var times = parseInt($("#pnumber").val()) + 1;
							for (var i = times; i < times + 5; i++) {
								var arr = new Array();
								arr.push("<tr><td>");
								arr.push(i);
								arr.push("</td>");
								arr
										.push("<td><input type='text' onfocus='pickProduct(this)' readonly='readonly' name='name");
								arr.push(i);
								arr.push("'/><input type='hidden' name='pid");
								arr.push(i);
								arr.push("'/><input type='hidden' name='remain");
								arr.push(i);
								arr.push("'/><input type='hidden' name='cid");
								arr.push(i);
								arr.push("'/></td>");
								arr
										.push("<td><input type='text' readonly='readonly' name='specification"+i+"'/></td>");
								arr
										.push("<td><input type='text' onblur='pnumblur(this)' reason='pnum' name='number"
												+ i + "'/></td>");
								arr
										.push("<td><input type='text' onblur='punitblur(this)' reason='punit' name='unitprice"
												+ i + "'/></td>");
								arr
										.push("<td><input type='text' readonly='readonly' reason='sum' name='sumprice"+i+"'/></td>");

								arr
										.push("<td><input type='button' value='删除' onclick='delproduct(this,-1)' /></td></tr>");

								$("#prod").append(arr.join(""));
							}
							$("#pnumber")
									.val(parseInt($("#pnumber").val()) + 5);
						});
		$("#sub").click(function() {
			addsum();
			var pnum = $("input[name^='number']");
			var index = 0;
			for (var i = 0; i < pnum.length; i++) {
				if (isinteger(pnum[i].value)) { // 查看这次商品一个做多少份货物
					index++;
				}
			}
			$("#pnumber").val(index);

		});
		$("#add").click(function() {
			addsum();
		});
	});
</script>
</head>

<body>
	<div class="alert alert-info">
		当前位置<b class="tip"></b>库存管理<b class="tip"></b>修改入库单
	</div>
	<form action="UpdateImport" method="post">
		<table class="tbform">
			<tr>
				<td class="tdl">日期</td>
				<td class="detail"><input name='date' type='text'
					onclick="SelectDate(this,'yyyy-MM-dd')"
					value='<%=StringUtil.date2str(im.getDate())%>' /></td>
				<td class="tdl">供应商</td>
				<td class="detail"><input name='supplier' type='text'
					value='<%=im.getSupplier()%>' /></td>
			</tr>
		</table>

		<%
			CommodityDAO cdao = new CommodityDAO();
			List<Commodity> commodities = cdao.select(id, 1);
		%>

		<input type='hidden' id='pnumber' name='pnumber'
			value="<%=commodities.size()%>" /> <input type='hidden' name='id'
			value='<%=id%>' />
		<table id="prod" class="tb">
			<tr>
				<td>序号</td>
				<td>产品名称</td>
				<td>规格</td>
				<td>数量</td>
				<td>单价</td>
				<td>总价</td>
				<td></td>
			</tr>
			<%
				int index = 1;
				Product product = null;
				ProductDAO pdao = new ProductDAO();
				WarehouseDAO wdao = new WarehouseDAO();
				for (Commodity commodity : commodities) {
					product = pdao.select(commodity.getPid());
					out.println("<tr><td>"
							+ index
							+ "</td><td><input onfocus='pickProduct(this)' name='name"
							+ index + "' type='text' readonly='readonly' value='"
							+ product.getName() + "'/>");
					out.println("<input name='pid" + index
							+ "' type='hidden' value='" + commodity.getPid()
							+ "'/>");
					out.println("<input name='remain" + index
							+ "' type='hidden' value='"
							+ wdao.select(commodity.getPid()).getNumber() + "'/>");
					out.println("<input name='cid" + index
							+ "' type='hidden' value='" + commodity.getId()
							+ "'/></td>");

					out.println("<td><input name='specification" + index
							+ "' type='text' readonly='readonly' value='"
							+ product.getSpecification() + "'/></td>");
					out.println("<td><input onblur='pnumblur(this)' reason='pnum' name='number"
							+ index
							+ "' type='text' value='"
							+ commodity.getNumber() + "'/></td>");
					out.println("<td><input onblur='punitblur(this)' reason='punit' name='unitprice"
							+ index
							+ "' type='text' value='"
							+ commodity.getUnitprice() + "'/></td>");
					out.println("<td><input reason='sum' readonly='readonly' name='sumprice"
							+ index
							+ "' type='text' value='"
							+ commodity.getSumprice() + "'/></td>");
					out.println("<td><input type='button' class='btn btn-mini btn-danger del' value='删除' onclick='delproduct(this,"
							+ commodity.getId() + ")'/> </td></tr>");

					index++;
				}
			%>
		</table>
		<table align="center">
			<tr>
				<td align="right"><input type="text" name="sum"
					value="<%=im.getSumprice()%>" id="sum" readonly="readonly" /> 
					<input class="btn btn-primary add" type="button" id="add" value="求总和" /> 
					<input type="button" class="btn btn-primary add" id="more" value="新增" /></td>
			</tr>

			<tr>
				<td><input class="btn btn-primary add"type="submit" id="sub" value="提交" /> 
				<input class="btn btn-primary add" type="button" onclick="javascript:window.close();" value="取消" />
				</td>
			</tr>
		</table>
		<input type="hidden" name="delids" id="delids" />
	</form>
</body>
</html>
