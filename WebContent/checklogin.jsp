<%@ page pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";

	String status = (String) session.getAttribute("status");

	if (!"login".equals(status)) {
		session.setAttribute("status", "logout");
		response.sendRedirect(basePath + "login.jsp");
	}
%>

