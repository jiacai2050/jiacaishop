<%@ page language="java"
	import="com.jiacai.bean.*,com.jiacai.dao.*,com.jiacai.util.*,java.util.*"
	pageEncoding="UTF-8"%>
<%@include file="../checklogin.jsp"%>
<%
	String id = request.getParameter("id");
	ProductDAO dao = new ProductDAO();
	Product product = dao.select(Integer.valueOf(id));
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>详细信息</title>
<base href="<%=basePath%>" />
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="style/admin-all.css" />
<link rel="stylesheet" type="text/css" href="style/base.css" />
<link rel="stylesheet" type="text/css"
	href="style/ui-lightness/jquery-ui-1.8.22.custom.css" />
<link rel="stylesheet" type="text/css" href="style/formui.css" />

<script type="text/javascript" src="js/jquery-1.7.2.js"></script>
<script type="text/javascript">
	function del() {
		if (confirm("确认删除货单？")) {
			$.ajax({
				url : "DeleteAjax",
				type : "get",
				dataType : "text",
				data : {
					"id" :
<%=id%>
	,
					"from" : "product"
				},
				success : function(res) {
					if ("1" == res) {
						alert("删除成功！");
						window.location = "product/add.jsp";
					} else {
						alert("删除失败！");
					}
				}
			});//ajax
		}
	}
</script>
</head>

<body>
	<div class="alert alert-info">
		当前位置<b class="tip"></b>商品管理<b class="tip"></b>商品信息
	</div>
	<table class="tbform" style="width:300px" align="center">
		<tr>
			<td class="tdl" align="right">名称</td>
			<td class="detail"><%=product.getName()%></td>
		</tr>
		<tr>	
			<td class="tdl"  align="right" >规格</t>
			<td class="detail"><%=product.getSpecification()%></td>
		</tr>	
		<tr>
			<td align="right"><a class="btn btn-primary add" href="product/edit.jsp?id=<%=id%>">修改</a></td>
			<td><input class="btn btn-primary add" type="button" onclick="del()" value="删除"></td>
		</tr>	
	</table>
</body>
</html>
