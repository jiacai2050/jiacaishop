<%@ page language="java"
	import="java.util.*,com.jiacai.bean.*,com.jiacai.dao.*,com.jiacai.util.*"
	pageEncoding="UTF-8"%>
<%@include file="../checklogin.jsp"%>
<%
	String id = request.getParameter("id");
	ProductDAO dao = new ProductDAO();
	Product product = dao.select(Integer.valueOf(id));
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>修改商品</title>
<base href="<%=basePath%>" />
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="style/admin-all.css" />
<link rel="stylesheet" type="text/css" href="style/base.css" />
<link rel="stylesheet" type="text/css"
	href="style/ui-lightness/jquery-ui-1.8.22.custom.css" />
<link rel="stylesheet" type="text/css" href="style/formui.css" />

<script type="text/javascript" src="js/jquery-1.7.2.js"></script>
<script type="text/javascript">
	$(function() {
		$("#cancel").click(function() {
			window.location="product/new.jsp";	
		});
	});
</script>
</head>

<body>
	<div class="alert alert-info">
		当前位置<b class="tip"></b>商品管理<b class="tip"></b>修改商品
	</div>
	<form action="UpdateProduct" method="post">
		<table class="tbform" style="width:300px" align="center">
			<tr>
				<td class="tdl" align="right">名称</td>
				<td class="detail"><input name='name' type='text'
					value='<%=product.getName()%>' /></td>
			<tr>
			<tr>
				<td class="tdl" align="right">规格</td>
				<td class="detail"><input name='specification' type='text'
					value='<%=product.getSpecification()%>' /></td>
			<tr>
			<tr>
				<td align="right"><input class="btn btn-primary add" type="submit" id="sub" value="提交修改" /></td>
				<td><input class="btn btn-primary add" type="button" id="cancel" value="取消修改" /></td>
			</tr>
		</table>
		<input type="hidden" name="id" id="id" value="<%=id%>" />
	</form>
</body>
</html>
