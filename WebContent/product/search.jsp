<%@ page language="java"
	import="com.jiacai.bean.*,com.jiacai.pagination.*,java.util.List"
	pageEncoding="UTF-8"%>
<%@include file="../checklogin.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>搜索页面</title>
<base href="<%=basePath%>" />
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<script type="text/javascript" src="js/jquery-1.7.2.js"></script>
<script type="text/javascript" src="js/calendar.js"></script>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/tb.js"></script>
<link rel="stylesheet" type="text/css" href="style/admin-all.css" />
<link rel="stylesheet" type="text/css" href="style/base.css" />
<link rel="stylesheet" type="text/css"
	href="style/ui-lightness/jquery-ui-1.8.22.custom.css" />
<link rel="stylesheet" type="text/css" href="style/formui.css" />

<script type="text/javascript">
	$('#list').hide();
	$('#find').click(function() {
		$('#list').show();
	});
	$(function() {
		var fromWhere = parent.document.getElementsByTagName('iframe');
		switch (fromWhere.length) {
		case 0:
		case 1:
			$('#from').val("index");
			break;
		case 2:
			$('#from').val("pick");
			break;
		default:
			$('#from').val("index");
		}
		$(".pager a").each(function(idx,obj) {
			var href = $(obj).attr("href");
			if(href != null) {
				$(obj).attr("href" , href + "&from=" + $("#from").val());
			}
		});
	});
	
	
</script>
</head>

<body>
	<div class="alert alert-info">
		当前位置<b class="tip"></b>订单管理<b class="tip"></b>搜索订单
	</div>
	<form action="SearchProduct">
		<table class="tbform">
			<tr>
				<td class="tdl">商品名称：</td>
				<td class="detail"><input type="text" name="name" /></td>
			</tr>
			<tr>
				<td class="tdl">商品规格：</td>
				<td class="detail"><input type="text" name="specification" /></td>
			</tr>

			<tr>
				<td class="tdl"></td>
				<td class="detail"><input type="submit" class="btn btn-primary add" id="find" value="搜索" /></td>
			</tr>
		</table>
		<input type="hidden" value="1" name="pageNo"> <input
			type="hidden" id="from" name="from" />
	</form>

	<!--result-->
	<table class="tb" id="list">
		<tbody>
			<tr>
				<td></td>
				<th>名称</th>
				<th>规格</th>
				<th>库存</th>
				<th></th>
			</tr>
		</tbody>
		<%
			ProductPage pageDiv = (ProductPage) request.getAttribute("result");
			List<Long> numbers = (List<Long>) request.getAttribute("number");
			String from = request.getParameter("from");
			if (null != pageDiv) {
				int length = pageDiv.getSumPage();
				int pageNo = pageDiv.getCurrentPage();
				if (length != 0) {
					List<Product> products = pageDiv.getProducts();
					for (int i = 0; i < products.size(); i++) {

						Product product = products.get(i);
						out.println("<tr><td>" + (i + 1) + "</td>");
						out.println("<td>" + product.getName() + "</td>");
						out.println("<td>" + product.getSpecification()
								+ "</td>");
						out.println("<td>" + numbers.get(i) + "</td>");
						if ("index".equals(from)) {
							out.println("<td><a class='btn btn-primary add' href=product/detail.jsp?id="
									+ product.getId() + ">详细信息</a></td>");
						} else {
							out.println("<td><input type='radio' name='productid' value='"
									+ product.getId() + "' /></td>");
						}
						out.println("</tr>");
					}

				} else {
					out.print("<font color='red'>没有符合您搜索要求的商品！</font>");
				}
				out.print("<tr class='pager'><th colspan='7'>当前是" + pageNo
						+ "页/共" + length + "页&nbsp;" + pageDiv.getPageNavi()
						+ "</th></tr>");
			}
		%>
	</table>
</body>
</html>
