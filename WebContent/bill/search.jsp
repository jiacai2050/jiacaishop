<%@ page language="java"
	import="com.jiacai.bean.*,com.jiacai.pagination.*,java.util.List,com.jiacai.util.*"
	pageEncoding="UTF-8"%>
<%@include file="../checklogin.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>搜索出库单</title>
<base href="<%=basePath%>" />
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<script type="text/javascript" src="js/jquery-1.7.2.js"></script>
<script type="text/javascript" src="js/calendar.js"></script>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.22.custom.min.js"></script>
<script type="text/javascript" src="js/tb.js"></script>
<link rel="stylesheet" type="text/css" href="style/admin-all.css" />
<link rel="stylesheet" type="text/css" href="style/base.css" />
<link rel="stylesheet" type="text/css"
	href="style/ui-lightness/jquery-ui-1.8.22.custom.css" />
<link rel="stylesheet" type="text/css" href="style/formui.css" />

<script type="text/javascript">
	$('#list').hide();
	$('#find').click(function() {
		$('#list').show();
	});
</script>
</head>

<body>
	<div class="alert alert-info">
		当前位置<b class="tip"></b>订单管理<b class="tip"></b>搜索账单
	</div>
	<form action="SearchBill">

		<table class="tbform">
			<tr>
				<td class="tdl">收货人：</td>
				<td class="detail"><input type="text" name="name" /></td>
				<td class="tdl">日期：</td>
				<td  colspan="2" class="detail">
					<input type="text" name="date1" onclick="SelectDate(this,'yyyy-MM-dd')" readonly="readonly" />
					-- 
					<input type="text" name="date2" onclick="SelectDate(this,'yyyy-MM-dd')" readonly="readonly" />
				</td>
			</tr>
			<tr>
				<td class="tdl">地址：</td>
				<td class="detail"><input type="text" name="address" /></td>
				<td class="tdl">电话：</td>
				<td class="detail"><input type="text" name="tel" /></td>
				<td class="detail"><input type="submit" class="btn btn-primary add" id="find" value="搜索" /></td>
			</tr>

		</table>
		<input type="hidden" value="1" name="pageNo">
	</form>

	<!--result-->
	<table class="tb" id="list">
		<tbody>
			<tr>
				<td></td>
				<th>日期</th>
				<th>收货人</th>
				<th>地址</th>
				<th>电话</th>
				<th>合计</th>
				<th></th>
			</tr>
		</tbody>
		<%
			GuestBillPage pageDiv = (GuestBillPage) request.getAttribute("result");
			String from = request.getParameter("from");
			
			if (null != pageDiv) {
				int length = pageDiv.getSumPage();
				int pageNo = pageDiv.getCurrentPage();
				if (length != 0) {
					Guest guest = null;
					Bill bill = null;
					List<GuestBill> guestbills  = pageDiv.getGuestBills();
					int billLen= guestbills.size();
					for (int i = 0; i < billLen; i++) {
						GuestBill guestBill  = guestbills.get(i);
						guest = guestBill.getGuest();
						bill = guestBill.getBill();
						out.println("<tr><td>" + (i + 1) + "</td>");
						out.println("<td>" + StringUtil.date2str(bill.getDate())
								+ "</td>");
						out.println("<td>" + guest.getName() + "</td>");
						out.println("<td>" + guest.getAddress() + "</td>");
						out.println("<td>" + guest.getTel() + "</td>");
						out.println("<td>" + bill.getSumprice() + "</td>");
						out.println("<td><a class='btn btn-primary add' href=bill/detail.jsp?id="
								+ bill.getId() + ">详细信息</a></td>");

						out.println("</tr>");
					}

				} else {
					out.print("<font color='red'>没有符合您搜索要求的订单！</font>");
				}
				out.print("<tr class='pager'><th colspan='7'>当前是" + pageNo
						+ "页/共" + length + "页&nbsp;" + pageDiv.getPageNavi()
						+ "</th></tr>");
			}
		%>
	</table>
</body>
</html>
