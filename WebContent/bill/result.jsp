<%@ page language="java" import="com.jiacai.util.*" pageEncoding="UTF-8"%>
<%@include file="../checklogin.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//Dth HTML 4.01 Transitional//EN">
<html>
<head>
<title>出库添加结果</title>
<base href="<%=basePath%>" />
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="style/admin-all.css" />
<link rel="stylesheet" type="text/css" href="style/base.css" />
<link rel="stylesheet" type="text/css"
	href="style/ui-lightness/jquery-ui-1.8.22.custom.css" />
<link rel="stylesheet" type="text/css" href="style/formui.css" />

</head>

<body>
	<div class="alert alert-info">
		当前位置<b class="tip"></b>账单管理<b class="tip"></b>操作反馈
	</div>
	<table class='tbform' align="center" >
		<%
			int res = StringUtil.str2int(request.getParameter("res"));
			int from = StringUtil.str2int(request.getParameter("from"));
			
			String[] kind = { "添加", "修改" };
			if (1 == res) {
				out.print("<tr><td align='center'>"+kind[from]+"成功</td></tr>");
				String id = request.getParameter("id");
				if(!StringUtil.nullOrEmpty(id)) {
					out.println("<tr><td align='center'><a href='bill/detail.jsp?id="+id+"'>查看刚才的出库单</a></td></tr>");	
				}
				
			} else {
				out.print("<tr><td align='center'>"+kind[from]+"失败</td></tr>");
			}
			
		%>
		<tr><td align='center'><a class="btn btn-primary add" href='bill/add.jsp'>填写新的账单</a></td></tr>
	</table>
</body>
</html>
