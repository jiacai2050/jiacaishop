<%@ page language="java"
	import="com.jiacai.bean.*,com.jiacai.dao.*,com.jiacai.util.*,java.util.*"
	pageEncoding="UTF-8"%>
<%@include file="../checklogin.jsp"%>
<%
	String id = request.getParameter("id");
BillDAO dao = new BillDAO();
GuestDAO gdao = new GuestDAO();
Bill bill = dao.select(StringUtil.str2int(id));
Guest guest = gdao.select(bill.getGid());
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>详细信息</title>
<base href="<%=basePath%>" />
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<script type="text/javascript" src="js/jquery-1.7.2.js"></script>
<script type="text/javascript" src="js/print.js"></script>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.22.custom.min.js"></script>
<script type="text/javascript" src="js/tb.js"></script>
<link rel="stylesheet" type="text/css" href="style/admin-all.css" />

<link rel="stylesheet" type="text/css"
	href="style/ui-lightness/jquery-ui-1.8.22.custom.css" />
<link rel="stylesheet" type="text/css" href="style/formui.css" />
<script type="text/javascript">
	function del() {
		if (confirm("确认删除货单？")) {
			$.ajax({
				url : "DeleteAjax",
				type : "get",
				dataType : "text",
				data : {
					"id" :
<%=id%>
	,
					"from" : "bill"
				},
				success : function(res) {
					if ("1" == res) {
						alert("删除成功！");
						window.location = "bill/add.jsp"
					} else {
						alert("删除失败！");
					}
				}
			});//ajax
		}
	}
</script>
</head>

<body>
	<div class="alert alert-info">
		当前位置<b class="tip"></b>订单管理<b class="tip"></b>账单详情
	</div>
	<!--startprint-->
	<center>
		<h1>家财电器销货清单</h1>
	</center>

	<table width='800px' align='center' border='1'>
		<tr>
			<th>日期</th>
			<td><%=StringUtil.date2str(bill.getDate())%></td>
			<th>姓名</th>
			<td><%=guest.getName()%></td>
			<th>地址</th>
			<td><%=guest.getAddress()%></td>
			<th>电话</th>
			<td><%=guest.getTel()%></td>
		</tr>
	</table>

	<table width='800px' align='center' border='1'>
		<tr>
			<td align="center">产品名称</td>
			<td align="center">规格</td>
			<td align="center">数量(台)</td>
			<td align="center">单价(元)</td>
			<td align="center">总价(元)</td>
		</tr>
		<%
			CommodityDAO cdao = new CommodityDAO();
			ProductDAO pdao = new ProductDAO();
			List<Commodity> commodities = cdao
					.select(StringUtil.str2int(id), 2);
			Product product = null;
			for (Commodity commodity : commodities) {
				int pid = commodity.getPid();
				product = pdao.select(pid);
				out.println("<tr><td align='center'>" + product.getName()
						+ "</td>");
				out.println("<td align='center'>" + product.getSpecification()
						+ "</td>");
				out.println("<td align='center'>" + commodity.getNumber()
						+ "</td>");
				out.println("<td align='center'>" + commodity.getUnitprice()
						+ "</td>");
				out.println("<td align='center'>" + commodity.getSumprice()
						+ "</td></tr>");
			}
		%>
		<tr align='right'>
			<td align="right" colspan='4'><font style='font-size: 24pt;'>合计</font></td>
			<td align='center'><font style='font-size: 24pt;'> <%=bill.getSumprice()%></font></td>
		</tr>
	</table>

	<table width='800px' align="center">
		<tr>
			<td colspan="2">
				主营：汽车充电机（12V、24V、36V、48V、60V、72V），逆变器（12V、24V、48V、60V）</td>
		</tr>
		<tr>
			<td colspan="2">
			二相、三相高精度稳压器，调压器，铜线尾，
				捕鱼机，捕兔器，电瓶检测表，电瓶卡头(铅、纯铜、合金)</td>
		</tr>
		<tr>
			<td colspan="2">
				大中小型充电夹（纯铜、镀铜、镀镍、镀锌），鳄鱼夹，电焊钳，焊机地线夹（300A、500A）</td>
		</tr>
		<tr>
			<td colspan="2">
				汽车启动连接线（2米、3米、4米、5米）</td>
		</tr>
		
		<tr>
			<td width="400px">
			工行免费：6222&nbsp;&nbsp;0816&nbsp;&nbsp;1000&nbsp;&nbsp;0476&nbsp;&nbsp;236
			</td>
			<td>户名：韩桂花</td>
		</tr>
		<tr>
			<td>
				农&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;行：95599&nbsp;&nbsp;8182&nbsp;&nbsp;17845&nbsp;&nbsp;69616
			</td>
			<td>户名：韩桂花</td>
		</tr>
		<tr>
			<td>
				
				信&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;合：6223&nbsp;&nbsp;1916&nbsp;&nbsp;2892&nbsp;&nbsp;0782
			</td>
			<td>户名：韩桂花</td>
		</tr>
		<tr>
			<td>
				电&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;话：0539-2937187&nbsp;&nbsp;&nbsp;&nbsp;7119087</td>
			<td>手机：13869909098</td>	
		</tr>


		<tr>
			<td>地&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;址：山东省临沂市河东区(原五金城西南区426号)</td>
			<td>QQ：1611174490</td>
		</tr>
		<tr>
			<td colspan="2" align="center"><h1>欢迎新老客户光临</h1></td>
		</tr>

	</table>

	<!--endprint-->

	<table align='center'>
		<tr>
			<td align="center"><input type="button"
				class="btn btn-primary add" value="打印" onclick="preview()"></td>
		</tr>
		<tr>
			<td><a class="btn btn-primary add"
				href="bill/edit.jsp?id=<%=id%>">修改</a> ---- <input
				class="btn btn-primary add" type="button" onclick="del()" value="删除"></td>
		</tr>
	</table>
</body>
</html>
