<%@ page language="java" pageEncoding="UTF-8"%>
<%@include file="../checklogin.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>添加货物</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">

<script type="text/javascript" src="../js/jquery-1.7.2.js"></script>
<script type="text/javascript" src="../js/calendar.js"></script>
<script type="text/javascript" src="../js/util.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.8.22.custom.min.js"></script>
<script type="text/javascript" src="../js/tb.js"></script>
<link rel="stylesheet" type="text/css" href="../style/admin-all.css" />
<link rel="stylesheet" type="text/css" href="../style/base.css" />
<link rel="stylesheet" type="text/css" href="../style/ui-lightness/jquery-ui-1.8.22.custom.css" />
<link rel="stylesheet" type="text/css" href="../style/formui.css" />


<script type="text/javascript">
	function autoadd(obj) {
		var addr = $("#address").val();
		var order = $("#order").val();
		$.ajax({
			url : "FindPhoneAjaxServlet",
			type : "POST",
			dataType : "text",
			data : {
				"addr" : addr,
				"order" : order
			},
			success : function(res) {
				$(obj).val(res);
			}
		});//ajax
	}
	
	$(function() {
		var now = new Date();
		var year = now.getFullYear();
		var month = now.getMonth();
		var date = now.getDate();
		$("#date").val(year + "-" + (month + 1) + "-" + date);

		var times = 1;
		$("#more")
				.click(
					  	function() {
							 
							for (var i = times; i < times + 5; i++) {
								var arr = new Array();
								arr.push("<tr><td>");
								arr.push(i);
								arr.push("</td>");
								arr.push("<td><input type='text' name='product");
								arr.push(i);
								arr.push("'/></td>");
								arr.push("<td><input type='text' name='specification");
								arr.push(i);
								arr.push("'/></td>");
								arr.push("<td><input type='text' onblur='pnumblur(this)' reason='pnum' name='number");
								arr.push(i);
								arr.push("'/></td>");
								arr.push("<td><input type='text' onblur='punitblur(this)' reason='punit' name='unitprice");
								arr.push(i);
								arr.push("'/></td>");
								arr.push("<td><input type='text' reason='sum' name='sumprice");
								arr.push(i);
								arr.push("'/></td></tr>");
								
								$("#prod").append(arr.join(""));
							}
							times += 5;
						}
					  );
		$("#more").click();
		$("#more").click();
		$("#sub").click(function() {
			addsum();
			var pnum = $("input[reason=pnum]");
			var index = 0;
			for (var i = 0; i < pnum.length; i++) {
				if (isinteger(pnum[i].value)) { // 查看这次商品一个做多少份货物
					index++;
				}
			}
			$("#pnumber").val(index);

		});
		$("#add").click(function() {
			addsum();
		});
	});
</script>
</head>

<body>
	<div class="alert alert-info">
		当前位置<b class="tip"></b>订单管理<b class="tip"></b>增加订单
	</div>
	<form action="AddServlet" method="get">
		<table class="tbform">
			<tbody>
				<tr>
					<td class="tdl">日期</td>
					<td class="detail"><input type="text" name="date" id="date"
						onclick="SelectDate(this,'yyyy-MM-dd')" readonly="readonly" /></td>

					<td class="tdl">地址</td>
					<td class="detail"><input id="address" type="text"
						name="address" /></td>
				</tr>
				<tr>
					<td class="tdl">收货人</td>
					<td class="detail"><input type="text" id="order" name="order" />
					</td>

					<td class="tdl">电话</td>
					<td class="detail"><input type="text" name="phone"
						onfocus="autoadd(this)" /></td>
				</tr>
			</tbody>
		</table>

			<table class="tb" id="prod">
				<tr>
					<th>序号</th>
					<th>产品名称</th>
					<th>规格</th>
					<th>数量</th>
					<th>单价</th>
					<th>总价</th>
				</tr>
			<!-- placeholder -->
			</table>
			<br />
			<table align="center">
				<tr>
					<td ><input type="text" style="width: 150px"
						name="sum" id="sum" readonly="readonly" /> <input type="button"
						id="add" value="求总和" /> <input type="button" id="more" value="新增" />
					</td>
				</tr>
				<tr>
					<td><input
						style="width: 150px; height: 75px; color: red; font-size: 25pt;"
						type="submit" id="sub" value="提交订单" /></td>
				</tr>
			</table>
			<input type="hidden" id="pnumber" name="pnumber" />
	</form>
</body>
</html>
