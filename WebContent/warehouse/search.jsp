<%@ page language="java"
	import="java.util.*,com.jiacai.bean.*,com.jiacai.util.*"
	pageEncoding="UTF-8"%>
<%@include file="../checklogin.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>搜索页面</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<script type="text/javascript" src="../js/jquery-1.7.2.js"></script>
<script type="text/javascript" src="../js/calendar.js"></script>
<script type="text/javascript" src="../js/util.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.8.22.custom.min.js"></script>
<script type="text/javascript" src="../js/tb.js"></script>
<link rel="stylesheet" type="text/css" href="../style/admin-all.css" />
<link rel="stylesheet" type="text/css" href="../style/base.css" />
<link rel="stylesheet" type="text/css"
	href="../style/ui-lightness/jquery-ui-1.8.22.custom.css" />
<link rel="stylesheet" type="text/css" href="../style/formui.css" />

<script type="text/javascript">
	$('#list').hide();
	$('#find').click(function() {
		$('#list').show();
	})
</script>
</head>

<body>
	<div class="alert alert-info">
		当前位置<b class="tip"></b>订单管理<b class="tip"></b>搜索订单
	</div>
	<form action="SearchServlet">
		<table class="tbform">
			<tr>
				<td class="tdl">收货人：</td>
				<td class="detail"><input type="text" name="order" /></td>
			</tr>
			<tr>
				<td class="tdl">地址：</td>
				<td class="detail"><input type="text" name="address" /></td>
			</tr>
			<tr>
				<td class="tdl">电话：</td>
				<td class="detail"><input type="text" name="phone" /></td>
			</tr>
			<tr>
				<td class="tdl">日期：</td>
				<td class="detail"><input type="text" name="date1"
					onclick="SelectDate(this,'yyyy-MM-dd')" readonly="readonly" />-- <input
					type="text" name="date2" style="width: 150px;"
					onclick="SelectDate(this,'yyyy-MM-dd')" readonly="readonly" /></td>
			</tr>
			<tr>
				<td class="tdl"></td>
				<td class="detail"><input type="submit" id="find" value="搜索" /></td>
			</tr>
		</table>
		<input type="hidden" value="1" name="pageNo"> <input
			type="hidden" value="jsp" name="fromwhere">
	</form>

	<!--result-->
	<table class="tb" id="list">
		<tbody>
			<tr>
				<td></td>
				<th>日期</th>
				<th>地址</th>
				<th>收货人</th>
				<th>电话</th>
				<th>总额</th>

				<th><input type="button"
					onclick="window.open('DownloadExcelServlet')" value="导出表格"></th>
			</tr>
		</tbody>
		<%
			WayBillPageDiv pageDiv = (WayBillPageDiv) request
					.getAttribute("result");
			if (null != pageDiv) {
				int length = pageDiv.getPageSum();
				int pageNo = pageDiv.getPageNo();
				if (length != 0) {
					List<Waybill> waybills = pageDiv.getWaybills();
					for (int i = 0; i < waybills.size(); i++) {

						Waybill waybill = waybills.get(i);
						out.println("<tr><td>" + (i + 1) + "</td>");
						out.println("<td>"
								+ StringUtil.date2str(waybill.getPdate())
								+ "</td>");
						out.println("<td>" + waybill.getAddress() + "</td>");
						out.println("<td>" + waybill.getOrdername() + "</td>");
						out.println("<td>" + waybill.getPhone() + "</td>");
						out.println("<td>" + waybill.getSum() + "</td>");
						out.println("<td><a target='_blank' href=detail.jsp?id="
								+ waybill.getId() + ">详细信息</a></td>");
						out.println("</tr>");
					}

				} else {
					out.print("没有符合您搜索要求的货单！");
				}
				out.print("<tr class='pager'><th colspan='7'>当前是"
						+ pageDiv.getPageNo() + "页/共" + pageDiv.getPageSum()
						+ "页&nbsp;" + pageDiv.getPageNavi() + "</th></tr>");
			}
		%>
	</table>
</body>
</html>
