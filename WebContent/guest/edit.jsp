<%@ page language="java"
	import="java.util.*,com.jiacai.bean.*,com.jiacai.dao.*,com.jiacai.util.*"
	pageEncoding="UTF-8"%>
<%@include file="../checklogin.jsp"%>
<%
	String id = request.getParameter("id");
	GuestDAO dao = new GuestDAO();
	Guest guest = dao.select(Integer.valueOf(id));
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>修改客户信息</title>
<base href="<%=basePath%>" />
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<link rel="stylesheet" type="text/css" href="style/admin-all.css" />
<link rel="stylesheet" type="text/css" href="style/base.css" />
<link rel="stylesheet" type="text/css" href="style/ui-lightness/jquery-ui-1.8.22.custom.css" />
<link rel="stylesheet" type="text/css" href="style/formui.css" />
<script type="text/javascript" src="js/jquery-1.7.2.js"></script>
<script type="text/javascript">
	$(function() {
		$("#cancel").click(function() {
			window.location="guest/add.jsp";	
		});
	});
</script>
</head>

<body>
		<div class="alert alert-info">
		当前位置<b class="tip"></b>客户管理<b class="tip"></b>修改客户信息
	</div>
	<form action="UpdateGuest" method="post" >
		<table class="tbform" style="width:300px" align="center">
			<tr>
				<td class="tdl" align="right">姓名</td>
				<td class="detail"><input name='name' type='text'
					value='<%=guest.getName()%>' /></td>
			<tr>
			<tr>
				<td class="tdl" align="right">地址</td>
				<td class="detail"><input name='address' type='text'
					value='<%=guest.getAddress()%>' /></td>
			<tr>
			<tr>
				<td class="tdl" align="right">电话</td>
				<td class="detail"><input name='tel' type='text'
					value='<%=guest.getTel()%>' /></td>
			<tr>
			<tr>
				<td align="right"><input class="btn btn-primary add" type="submit" id="sub" value="提交修改" /></td>
				<td><input class="btn btn-primary add" type="button" id="cancel" value="取消修改" /></td>
			</tr>
		</table>
		<input type="hidden" name="id" id="id" value="<%=id%>" />
	</form>
</body>
</html>
