<%@ page language="java"
	import="com.jiacai.bean.*,com.jiacai.pagination.*,java.util.List"
	pageEncoding="UTF-8"%>
<%@include file="../checklogin.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>搜索客户</title>
<base href="<%=basePath%>"/>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">

<script type="text/javascript" src="js/jquery-1.7.2.js"></script>
<script type="text/javascript" src="js/calendar.js"></script>
<script type="text/javascript" src="js/tb.js"></script>
<link rel="stylesheet" type="text/css" href="style/admin-all.css" />
<link rel="stylesheet" type="text/css" href="style/base.css" />
<link rel="stylesheet" type="text/css"
	href="style/ui-lightness/jquery-ui-1.8.22.custom.css" />
<link rel="stylesheet" type="text/css" href="style/formui.css" />

<script type="text/javascript">
	$('#list').hide();
	$('#find').click(function() {
		$('#list').show();
	});
	$(function() {
		var fromWhere = parent.document.getElementsByTagName('iframe');
		console.log(fromWhere.length);
		switch (fromWhere.length) {
		case 0:
		case 1:
			$('#from').val("index");
			break;
		case 2:
			$('#from').val("pick");
			break;
		default:
			$('#from').val("index");
		}
		$(".pager a").each(function(idx,obj) {
			var href = $(obj).attr("href");
			if(href != null) {
				$(obj).attr("href" , href + "&from=" + $("#from").val());
			}
		});
	});
	
</script>
</head>

<body>
	<div class="alert alert-info">
		当前位置<b class="tip"></b>客户管理<b class="tip"></b>搜索客户
	</div>
	<form action="SearchGuest">
		<table class="tbform">
			<tr>
				<td class="tdl">姓名：</td>
				<td class="detail"><input type="text" name="name" /></td>
				<td class="tdl">电话：</td>
				<td class="detail"><input type="text" name="tel" /></td>
			</tr>
			<tr>
				<td class="tdl">地址：</td>
				<td class="detail"><input type="text" name="specification" /></td>
				<td class="tdl"></td>
				<td class="detail"><input type="submit"  class="btn btn-primary add" id="find" value="搜索" /></td>
			</tr>
		</table>
		<input type="hidden" value="1" name="pageNo"> 
		 <input type="hidden" id="from" name="from" />
	</form>

	<!--result-->
	<table class="tb" id="list">
		<tbody>
			<tr>
				<td></td>
				<th>姓名</th>
				<th>电话</th>
				<th>地址</th>
				<th></th>
			</tr>
		</tbody>
		<%
		 
		GuestPage pageDiv = (GuestPage) request
					.getAttribute("result");
		String from = request.getParameter("from");
			if (null != pageDiv) {
				int length = pageDiv.getSumPage();
				int pageNo = pageDiv.getCurrentPage();
				if (length != 0) {
					List<Guest> guests = pageDiv.getGuests();
					for (int i = 0; i < guests.size(); i++) {

						Guest guest  = guests.get(i);
						out.println("<tr><td>" + (i + 1) + "</td>");
						out.println("<td>" + guest.getName() + "</td>");
						out.println("<td>" + guest.getTel() + "</td>");
						out.println("<td>"+guest.getAddress()+"</td>");
						if ("index".equals(from)) {
							out.println("<td><a class='btn' href=guest/detail.jsp?id="
									+ guest.getId() + ">详细信息</a></td>");;
						} else {
							out.println("<td><input type='radio' name='guestid' value='"
									+ guest.getId() + "' /></td>");
						}
						
						out.println("</tr>");
					}

				} else {
					out.print("<font color='red'>没有符合您搜索要求的客户！</font>");
				}
				out.print("<tr class='pager'><th colspan='7'>当前是"
						+ pageNo + "页/共" + length
						+ "页&nbsp;" + pageDiv.getPageNavi() + "</th></tr>");
			}
		%>
	</table>
</body>
</html>
