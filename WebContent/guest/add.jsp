<%@ page language="java" pageEncoding="UTF-8"%>
<%@include file="../checklogin.jsp"%>
 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
 
<title>添加货物</title>
<base href="<%=basePath%>" />
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">

<script type="text/javascript" src="js/jquery-1.7.2.js"></script>
<script type="text/javascript" src="js/calendar.js"></script>
<script type="text/javascript" src="js/util.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.22.custom.min.js"></script>
<script type="text/javascript" src="js/tb.js"></script>
<link rel="stylesheet" type="text/css" href="style/admin-all.css" />
<link rel="stylesheet" type="text/css" href="style/base.css" />
<link rel="stylesheet" type="text/css" href="style/ui-lightness/jquery-ui-1.8.22.custom.css" />
<link rel="stylesheet" type="text/css" href="style/formui.css" />


<script type="text/javascript">
	
	
	$(function() {
		var times = 1;
		$("#more")
				.click(
					  	function() {
							 
							for (var i = times; i < times + 5; i++) {
								var arr = new Array();
								arr.push("<tr><td>");
								arr.push(i);
								arr.push("</td>");
								arr.push("<td><input type='text' name='name");
								arr.push(i);
								arr.push("'/></td>");
								arr.push("<td><input type='text' name='address");
								arr.push(i);
								arr.push("'/></td>");
								arr.push("<td><input type='text' name='tel");
								arr.push(i);
								arr.push("'/></td></tr>");
								
								$("#prod").append(arr.join(""));
							}
							times += 5;
						}
					  );
		$("#more").click();
		$("#more").click();
		$("#sub").click(function() {
			var spec = $("input[name^='name']");
			var index = 0;
			for (var i = 0; i < spec.length; i++) {
				if (spec[i].value.trim()!="") { // 查看这次商品一个做多少份货物
					index++;
				}
			}
			$("#pnumber").val(index);
		});
	});
</script>
</head>

<body>
	<div class="alert alert-info">
		当前位置<b class="tip"></b>客户管理<b class="tip"></b>新增客户
	</div>
	<form action="AddGuest" method="post">
			<table class="tb" id="prod" style="width:250px" align="center">
				<tr>
					<th>序号</th>
					<th>姓名</th>
					<th>地址</th>
					<th>电话</th>
				</tr>
			<!-- placeholder -->
			</table>
			<br />
			<table  align="center" >
				<tr>
					<td><input class="btn btn-primary add" type="submit" id="sub" value="提交" /></td>
					<td>------</td>
					<td><input class="btn btn-primary add" type="button" id="more" value="新增" />
					</td>
				</tr>
			</table>
			<input type="hidden" id="pnumber" name="pnumber" />
	</form>
</body>
</html>
