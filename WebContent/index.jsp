﻿<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>后台管理系统</title>
<link rel="stylesheet" type="text/css" href="style/admin-all.css" />
<link rel="stylesheet" type="text/css" href="style/base.css" />
<link rel="stylesheet" type="text/css" href="style/bootstrap.min.css" />
<link rel="stylesheet" type="text/css"
	href="style/ui-lightness/jquery-ui-1.8.22.custom.css" />
<script type="text/javascript" src="js/jquery-1.7.2.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.22.custom.min.js"></script>
<script type="text/javascript" src="js/index.js"></script>
<script type="text/javascript">
	function decode(s) {
		return unescape(s.replace(/\\(u[0-9a-fA-F]{4})/gm, '%$1'));
	}
</script>

</head>
<body>
	<div class="warp">
		<!--头部开始-->
		<div class="top_c">
			<div class="top-menu">
				<ul class="top-menu-nav">
					<li><a href="#">首页</a></li>
					<li><a href="#">查询界面<i class="tip-up"></i></a>
						<ul class="kidc">
							<li><a target="Conframe" href="bill/search.jsp">订单查询</a></li>
							<li><a target="Conframe" href="import/search.jsp">进货查询</a></li>
							<li><a target="Conframe" href="product/search.jsp">库存查询</a></li>
						</ul></li>
					<li><a href="#">增加界面<i class="tip-up"></i></a>
						<ul class="kidc">
							<li><b class="tip"></b><a target="Conframe"
								href="bill/add.jsp">添加订单</a></li>
							<li><b class="tip"></b><a target="Conframe"
								href="import/add.jsp">添加进货</a></li>
							<li><b class="tip"></b><a target="Conframe"
								href="product/add.jsp">添加商品</a></li>
						</ul></li>
				</ul>
			</div>
			<div class="top-nav">
					欢迎您，家财电器！&nbsp;&nbsp;| <a  href="javascript:self.close()">安全退出</a>
			</div>
		</div>
		<!--头部结束-->
		<!--左边菜单开始-->
		<div class="left_c left">
			<h1>系统操作菜单</h1>
			<div class="acc">
				<div>
					<a class="one">订单管理</a>
					<ul class="kid">
						<li><b class="tip"></b><a target="Conframe"
							href="bill/add.jsp">添加订单</a></li>
						<li><b class="tip"></b><a target="Conframe"
							href="bill/search.jsp">订单查询</a></li>
					</ul>
				</div>
				<div>
					<a class="one">进货管理</a>
					<ul class="kid">
						<li><b class="tip"></b><a target="Conframe"
							href="import/add.jsp">添加进货</a></li>
						<li><b class="tip"></b><a target="Conframe"
							href="import/search.jsp">进货查询</a></li>
					</ul>
				</div>
				<div>
					<a class="one">商品库存管理</a>
					<ul class="kid">
						<li><b class="tip"></b><a target="Conframe"
							href="product/add.jsp">增加商品</a></li>
						<li><b class="tip"></b><a target="Conframe"
							href="product/search.jsp">查询库存</a></li>
					</ul>
				</div>
				<div>
					<a class="one">客户管理</a>
					<ul class="kid">
						<li><b class="tip"></b><a target="Conframe"
							href="guest/add.jsp">增加客户</a></li>
						<li><b class="tip"></b><a target="Conframe"
							href="guest/search.jsp">搜索客户</a></li>
					</ul>
				</div>
				<div id="datepicker"></div>
			</div>

		</div>
		<!--左边菜单结束-->
		<!--右边框架开始-->
		<div class="right_c">
			<div class="nav-tip" onclick="javascript:void(0)">&nbsp;</div>

		</div>
		<div class="Conframe">
			<iframe src="bill/add.jsp" name="Conframe" id="Conframe"></iframe>
		</div>
		<!--右边框架结束-->

		<!--底部开始-->
		<div class="bottom_c">Copyright &copy;2014 家财电器 版权所有</div>
		<!--底部结束-->
	</div>
</body>
</html>
