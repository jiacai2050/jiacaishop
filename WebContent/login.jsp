﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<head>
<base href="<%=basePath%>" />
<title>家财电器仓库管理系统</title>
<link rel="stylesheet" type="text/css" href="style/base.css" />
<link rel="stylesheet" type="text/css" href="style/admin-all.css" />
<link rel="stylesheet" type="text/css" href="style/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="style/login.css" />
<script type="text/javascript" src="js/jquery-1.7.2.js"></script>
<script type="text/javascript" src="js/jquery.spritely-0.6.js"></script>
<script type="text/javascript" src="js/chur.min.js"></script>

<script type="text/javascript">
	$(function() {
		$('#clouds').pan({
			fps : 20,
			speed : 0.7,
			dir : 'right',
			depth : 10
		});
		$('#sumbitBtn').click(function() {
			if ($('#username').val() == "" || $('#pwd').val() == "") {
				$('.tip').html('用户名或密码不可为空！');
				return false;

			}

		})
	})
	function setTip(msg) {
		//$('.tip').html(msg);
		alert(msg);
	}
</script>
</head>
<body>
	<%
		String status = (String) session.getAttribute("status");
		if (status != null) {
			if (status.equals("error")) {
				out.println("<script>setTip('密码输入错误，请重新输入！');</script>");
			} else if (status.equals("logout")) {
				out.println("<script>setTip('请先登录再进行相关操作！');</script>");
			} else if (status.equals("noname")) {
				out.println("<script>setTip('没有您输入的用户名！');</script>");
			}
		}
	%>
	<div id="clouds" class="stage"></div>
	<div class="loginmain"></div>
	<div class="row-fluid">
		<h1>家财电器仓库管理系统</h1>
		<form action="CheckLogin" method="post">
			<p>
				<label>帐&nbsp;&nbsp;&nbsp;号：<input type="text"
					name="username" id="username" value="admin" /></label>
			</p>
			<p>
				<label>密&nbsp;&nbsp;&nbsp;码：<input type="password"
					name="pwd" id="pwd" value="101010"/></label>
			</p>
		<!--
			<p>
				<label>权&nbsp;&nbsp;&nbsp;限： <select>
						<option value="volvo">老板</option>
						<option value="saab">员工</option>
				</select>
				</label>
			</p>
		 -->	
			<p class="tip">&nbsp;</p>
			<hr />
			<input type="submit" id="sumbitBtn" value=" 登 录 "
				class="btn btn-primary btn-large login" /> &nbsp;&nbsp;&nbsp;<input
				type="reset" value=" 重 置 " class="btn btn-large" />
		</form>
	</div>
</body>
</html>
