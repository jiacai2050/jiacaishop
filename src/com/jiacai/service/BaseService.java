package com.jiacai.service;

import javax.servlet.http.HttpServletRequest;

import com.jiacai.util.Table;

public abstract class BaseService {

	public String genPageSql(Table table,String whereClause, int currentPage,
			int numberPerPage) {
		String sql = "select SQL_CALC_FOUND_ROWS * from " + table
				+ whereClause + " limit " + (currentPage - 1) * numberPerPage
				+ "," + numberPerPage;
		return sql;
	}
	abstract public Object genPagination(HttpServletRequest request,
			int numberPerPage);
	abstract public String[] constructWhereAndUrl(HttpServletRequest request);
}
