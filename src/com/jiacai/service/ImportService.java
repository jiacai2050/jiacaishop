package com.jiacai.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.jiacai.bean.Import;
import com.jiacai.dao.ImportDAO;
import com.jiacai.pagination.ImportPage;
import com.jiacai.pagination.ProductPage;
import com.jiacai.util.StringUtil;
import com.jiacai.util.Table;

public class ImportService extends BaseService {

	@Override
	public ImportPage genPagination(HttpServletRequest request,
			int numberPerPage) {

		int currentPage = StringUtil.str2int(request.getParameter("pageNo"));

		String[] whereAndUrl = constructWhereAndUrl(request);
		String whereClause = whereAndUrl[0];
		String sql = this.genPageSql(Table.IMPORT, whereClause, currentPage,
				numberPerPage);

		ImportDAO dao = new ImportDAO();
		List<Import> imports = dao.executeQuery(sql);

		int totalRow = dao.getTotalRow();
		String[] naviAndTotal = StringUtil.genPageNavi("SearchImport",
				totalRow, numberPerPage, currentPage, whereAndUrl[1]);

		return new ImportPage(currentPage, numberPerPage,
				StringUtil.str2int(naviAndTotal[1]), naviAndTotal[0], imports);

	}

	@Override
	public String[] constructWhereAndUrl(HttpServletRequest request) {
		String supplier = request.getParameter("supplier");
		String date1 = request.getParameter("date1");
		String date2 = request.getParameter("date2");

		StringBuilder whereClause = new StringBuilder();
		StringBuilder urlParam = new StringBuilder();

		whereClause.append(" where 1=1");

		if (!StringUtil.nullOrEmpty(supplier)) {
			whereClause.append(" and supplier like '%" + supplier + "%'");
			urlParam.append("&supplier=" + supplier);
		}

		if (!StringUtil.nullOrEmpty(date1) && StringUtil.nullOrEmpty(date2))// 只有起始日期
		{
			whereClause.append(" and date = '" + date1 + "'");
			urlParam.append("&date1=" + date1);

		}
		if (StringUtil.nullOrEmpty(date1) && !StringUtil.nullOrEmpty(date2))// 只有终止日期
		{
			whereClause.append(" and date = '" + date2 + "'");
			urlParam.append("&date1=" + date1);
		}
		if (!StringUtil.nullOrEmpty(date1) && !StringUtil.nullOrEmpty(date2)) 
		{
			whereClause.append(" and date between '" + date1 + "' and '"
					+ date2 + "'");
			urlParam.append("&date1=" + date1 + "&date2=" + date2);
		}
		return new String[] { whereClause.toString(), urlParam.toString() };
	}

}
