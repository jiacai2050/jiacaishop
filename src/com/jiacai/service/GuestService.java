package com.jiacai.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.jiacai.bean.Guest;
import com.jiacai.dao.GuestDAO;
import com.jiacai.pagination.GuestPage;
import com.jiacai.pagination.ProductPage;
import com.jiacai.util.StringUtil;
import com.jiacai.util.Table;

public class GuestService extends BaseService{

	@Override
	public GuestPage genPagination(HttpServletRequest request, int numberPerPage) {
		int currentPage = StringUtil.str2int(request.getParameter("pageNo"));

		String[] whereAndUrl = constructWhereAndUrl(request);
		String whereClause = whereAndUrl[0];
		String sql = this.genPageSql(Table.GUEST, whereClause, currentPage,
				numberPerPage);
		
		GuestDAO dao = new GuestDAO();
		List<Guest> guests  = dao.executeQuery(sql);

		int totalRow = dao.getTotalRow();
		String[] naviAndTotal = StringUtil.genPageNavi("SearchGuest",
				totalRow, numberPerPage, currentPage, whereAndUrl[1]);

		return new GuestPage(currentPage, numberPerPage,
				StringUtil.str2int(naviAndTotal[1]), naviAndTotal[0], guests);
	}

	@Override
	public String[] constructWhereAndUrl(HttpServletRequest request) {
		String name = request.getParameter("name");
		String address = request.getParameter("address");
		String tel = request.getParameter("tel");

		StringBuilder whereClause = new StringBuilder();
		StringBuilder urlParam = new StringBuilder();

		whereClause.append(" where 1=1");
		if (!StringUtil.nullOrEmpty(name)) {
			whereClause.append(" and name like '%" + name + "%'");
			urlParam.append("&name=" + name);
		}
		if (!StringUtil.nullOrEmpty(address)) {
			whereClause.append(" and address like '%" + address
					+ "%'");
			urlParam.append("&address=" + address);
		}
		if (!StringUtil.nullOrEmpty(tel)) {
			whereClause.append(" and tel like '%" + tel
					+ "%'");
			urlParam.append("&tel=" + tel);
		}
		return new String[] { whereClause.toString(), urlParam.toString() };
		
	}

}
