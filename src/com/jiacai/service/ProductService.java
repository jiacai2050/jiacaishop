package com.jiacai.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.jiacai.bean.Product;
import com.jiacai.dao.ProductDAO;
import com.jiacai.pagination.ProductPage;
import com.jiacai.util.StringUtil;
import com.jiacai.util.Table;

public class ProductService extends BaseService {

	@Override
	public ProductPage genPagination(HttpServletRequest request,
			int numberPerPage) {

		int currentPage = StringUtil.str2int(request.getParameter("pageNo"));

		String[] whereAndUrl = constructWhereAndUrl(request);
		String whereClause = whereAndUrl[0];
		String sql = this.genPageSql(Table.PRODUCT, whereClause, currentPage,
				numberPerPage);
		// System.out.println(sql);
		ProductDAO dao = new ProductDAO();
		List<Product> products = dao.executeQuery(sql);

		int totalRow = dao.getTotalRow();
		String[] naviAndTotal = StringUtil.genPageNavi("SearchProduct",
				totalRow, numberPerPage, currentPage, whereAndUrl[1]);

		return new ProductPage(currentPage, numberPerPage,
				StringUtil.str2int(naviAndTotal[1]), naviAndTotal[0], products);

	}

	@Override
	public String[] constructWhereAndUrl(HttpServletRequest request) {
		String name = request.getParameter("name");
		String specification = request.getParameter("specification");

		StringBuilder whereClause = new StringBuilder();
		StringBuilder urlParam = new StringBuilder();

		whereClause.append(" where 1=1");
		if (!StringUtil.nullOrEmpty(name)) {
			whereClause.append(" and name like '%" + name + "%'");
			urlParam.append("&name=" + name);
		}
		if (!StringUtil.nullOrEmpty(specification)) {
			whereClause.append(" and specification like '%" + specification
					+ "%'");
			urlParam.append("&specification=" + specification);
		}
		return new String[] { whereClause.toString(), urlParam.toString() };
	}

}
