package com.jiacai.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.jiacai.bean.GuestBill;
import com.jiacai.dao.GuestBillDAO;
import com.jiacai.pagination.GuestBillPage;
import com.jiacai.util.StringUtil;
import com.jiacai.util.Table;

public class GuestBillService extends BaseService{

	@Override
	public GuestBillPage genPagination(HttpServletRequest request,
			int numberPerPage) {

		int currentPage = StringUtil.str2int(request.getParameter("pageNo"));

		String[] whereAndUrl = constructWhereAndUrl(request);
		String whereClause = whereAndUrl[0];
		//这里查的是视图
		String sql = this.genPageSql(Table.GUESTBILL, whereClause, currentPage,
				numberPerPage);

		GuestBillDAO dao = new GuestBillDAO();
		List<GuestBill> guestBills = dao.executeQuery(sql);

		int totalRow = dao.getTotalRow();
		String[] naviAndTotal = StringUtil.genPageNavi("SearchBill",
				totalRow, numberPerPage, currentPage, whereAndUrl[1]);

		return new GuestBillPage(currentPage, numberPerPage,
				StringUtil.str2int(naviAndTotal[1]), naviAndTotal[0], guestBills);

	}

	@Override
	public String[] constructWhereAndUrl(HttpServletRequest request) {
		String name = request.getParameter("name");
		String address = request.getParameter("address");
		String tel = request.getParameter("address");
		String date1 = request.getParameter("date1");
		String date2 = request.getParameter("date2");

		StringBuilder whereClause = new StringBuilder();
		StringBuilder urlParam = new StringBuilder();

		whereClause.append(" where 1=1");

		if (!StringUtil.nullOrEmpty(name)) {
			whereClause.append(" and name like '%" + name + "%'");
			urlParam.append("&name=" + name);
		}
		if (!StringUtil.nullOrEmpty(address)) {
			whereClause.append(" and address like '%" + address + "%'");
			urlParam.append("&address=" + address);
		}
		
		if (!StringUtil.nullOrEmpty(tel)) {
			whereClause.append(" and tel like '%" + tel + "%'");
			urlParam.append("&tel=" + tel);
		}

		if (!StringUtil.nullOrEmpty(date1) && StringUtil.nullOrEmpty(date2))// 只有起始日期
		{
			whereClause.append(" and date = '" + date1 + "'");
			urlParam.append("&date1=" + date1);

		}
		if (StringUtil.nullOrEmpty(date1) && !StringUtil.nullOrEmpty(date2))// 只有终止日期
		{
			whereClause.append(" and date = '" + date2 + "'");
			urlParam.append("&date1=" + date1);
		}
		if (!StringUtil.nullOrEmpty(date1) && !StringUtil.nullOrEmpty(date2)) 
		{
			whereClause.append(" and date between '" + date1 + "' and '"
					+ date2 + "'");
			urlParam.append("&date1=" + date1 + "&date2=" + date2);
		}
		return new String[] { whereClause.toString(), urlParam.toString() };
	}
}
