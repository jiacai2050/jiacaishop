package com.jiacai.bean;

public class User {

	private int id;
	private int role;
	private String name;
	private String pwd;

	public User() {
		super();
	}

	public User(int role, String name, String pwd) {
		this.role = role;
		this.name = name;
		this.pwd = pwd;
	}

	public User(int id, int role, String name, String pwd) {
		this(role, name, pwd);
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getRole() {
		return role;
	}

	public void setRole(int role) {
		this.role = role;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
}
