package com.jiacai.bean;

import java.util.Date;

public class Bill {

	private int id;
	private int gid;
	private Date date;
	private double sumprice;
	
	
	public Bill(int gid, Date date, double sumprice) {
		this.gid = gid;
		this.date = date;
		this.sumprice = sumprice;
	}
	public Bill(int id, int gid, Date date, double sumprice) {
		this.id = id;
		this.gid = gid;
		this.date = date;
		this.sumprice = sumprice;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getGid() {
		return gid;
	}
	public void setGid(int gid) {
		this.gid = gid;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public double getSumprice() {
		return sumprice;
	}
	public void setSumprice(double sumprice) {
		this.sumprice = sumprice;
	}
}
