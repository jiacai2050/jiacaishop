package com.jiacai.bean;

import java.util.Date;

public class Import {

	private int id;
	private int uid;
	private Date date;
	private String supplier;
	private double sumprice;

	public Import(int uid, Date date, String supplier, double sumprice) {
		this.uid = uid;
		this.date = date;
		this.supplier = supplier;
		this.sumprice = sumprice;
	}

	public Import(int id, int uid, Date date, String supplier, double sumprice) {
		this(uid, date, supplier, sumprice);
		this.id = id;

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public double getSumprice() {
		return sumprice;
	}

	public void setSumprice(double sumprice) {
		this.sumprice = sumprice;
	}
}