package com.jiacai.bean;

import java.util.List;

public class WayBillPageDiv {
 
	private List<Waybill> waybills;
	private String pageNavi;
	private int pageNo;
	private int pageSum;
	
	public WayBillPageDiv() {
		super();
	}
	public WayBillPageDiv(List<Waybill> waybills, String pageNavi,
			int pageNo, int pageSum) {
		super();
		this.waybills = waybills;
		this.pageNavi = pageNavi;
		this.pageNo = pageNo;
		this.pageSum = pageSum;
	}

	public List<Waybill> getWaybills() {
		return waybills;
	}

	public void setWaybills(List<Waybill> waybills) {
		this.waybills = waybills;
	}

	public String getPageNavi() {
		return pageNavi;
	}

	public void setPageNavi(String pageNavi) {
		this.pageNavi = pageNavi;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getPageSum() {
		return pageSum;
	}

	public void setPageSum(int pageSum) {
		this.pageSum = pageSum;
	}
	 
	
	
}
