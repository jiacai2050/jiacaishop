package com.jiacai.bean;

public class Price {

	private int gid;
	private int pid;
	private double unitprice;

	public Price() {
	}

	public Price(int gid, int pid, double unitprice) {
		this.gid = gid;
		this.pid = pid;
		this.unitprice = unitprice;
	}

	public int getGid() {
		return gid;
	}

	public void setGid(int gid) {
		this.gid = gid;
	}

	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public double getUnitprice() {
		return unitprice;
	}

	public void setUnitprice(double unitprice) {
		this.unitprice = unitprice;
	}

}
