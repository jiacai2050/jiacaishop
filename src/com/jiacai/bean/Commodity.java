package com.jiacai.bean;

public class Commodity {

	private int id;
	private int fid;
	private int kind;
	private int pid;
	private int number;
	private double unitprice;
	private double sumprice;
	
	public Commodity(int id, int fid, int kind, int pid, int number, double unitprice,
			double sumprice) {
		this.id = id;
		this.fid = fid;
		this.kind = kind;
		this.pid = pid;
		this.number = number;
		this.unitprice = unitprice;
		this.sumprice = sumprice;
	}
	
	public Commodity(int fid, int kind, int pid, int number, double unitprice,
			double sumprice) {
		this.fid = fid;
		this.kind = kind;
		this.pid = pid;
		this.number = number;
		this.unitprice = unitprice;
		this.sumprice = sumprice;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getFid() {
		return fid;
	}
	public void setFid(int fid) {
		this.fid = fid;
	}
	public int getKind() {
		return kind;
	}
	public void setKind(int kind) {
		this.kind = kind;
	}
	public int getPid() {
		return pid;
	}
	public void setPid(int pid) {
		this.pid = pid;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public double getUnitprice() {
		return unitprice;
	}
	public void setUnitprice(double unitprice) {
		this.unitprice = unitprice;
	}
	public double getSumprice() {
		return sumprice;
	}
	public void setSumprice(double sumprice) {
		this.sumprice = sumprice;
	}
	
	
}
