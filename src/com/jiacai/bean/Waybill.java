package com.jiacai.bean;

import java.io.Serializable;
import java.util.Date;

import com.jiacai.util.StringUtil;

public class Waybill implements Serializable{
 
	private static final long serialVersionUID = 1L;
	private int id;
	private String ordername;
	private String phone;
	private String address;
	private int pnum; //这次订单的商品数量
	private double sum;
	private Date pdate;
	
	public Waybill() {
		super();
	}

	public Waybill(String ordername, String phone, String address, int pnum,
			double sum, Date pdate) {
		
		this.ordername = StringUtil.null2Str(ordername);
		this.phone = StringUtil.null2Str(phone);
		this.address = StringUtil.null2Str(address);
		this.pnum = pnum;
		this.sum = sum;
		this.pdate = pdate;
	}
	public Waybill(int id, String ordername, String phone, String address, int pnum,
			double sum, Date pdate) {
		this(ordername, phone, address, pnum, sum, pdate);
		this.id = id;
	}
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOrdername() {
		return ordername;
	}

	public void setOrdername(String ordername) {
		this.ordername = ordername;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getPnum() {
		return pnum;
	}

	public void setPnum(int pnum) {
		this.pnum = pnum;
	}

	public double getSum() {
		return sum;
	}

	public void setSum(double sum) {
		this.sum = sum;
	}

	public Date getPdate() {
		return pdate;
	}

	public void setPdate(Date pdate) {
		this.pdate = pdate;
	}
	
	
}
