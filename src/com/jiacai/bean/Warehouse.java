package com.jiacai.bean;

public class Warehouse {

	private int pid;
	private long number;
	
	public Warehouse(int pid, long number) {
		this.pid = pid;
		this.number = number;
	}
	
	public int getPid() {
		return pid;
	}
	public void setPid(int pid) {
		this.pid = pid;
	}
	public long getNumber() {
		return number;
	}
	public void setNumber(long number) {
		this.number = number;
	}
	
	
	
}
