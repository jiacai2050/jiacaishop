package com.jiacai.bean;

import java.io.Serializable;

import com.jiacai.util.StringUtil;
import com.mysql.jdbc.StringUtils;

public class Product implements Serializable{
 
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String name;
	private String specification;
	
	public Product() {
		super();
	}

	public Product(String name, String specification) {
		
		this.name =StringUtil.null2Str(name);
		this.specification = StringUtil.null2Str(specification);
		
	}
	
	

	public Product(int id, String name, String specification) {
		this.id = id;
		this.name = name;
		this.specification = specification;
	}

	public int getId() {
		return id;
	}
 
	public String getName() {
		return name;
	}

	public void setName(String name) {
		name = StringUtil.null2Str(name);
		this.name = name;
	}

	public String getSpecification() {
		return specification;
	}

	public void setSpecification(String specification) {
		specification = StringUtil.null2Str(specification);
		this.specification = specification;
	}
 
}
