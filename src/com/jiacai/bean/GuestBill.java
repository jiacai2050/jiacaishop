package com.jiacai.bean;

public class GuestBill {
	//��ͼ
	private Guest guest;
	private Bill bill;

	public GuestBill() {
	}

	public GuestBill(Guest guest, Bill bill) {
		this.guest = guest;
		this.bill = bill;
	}

	public Guest getGuest() {
		return guest;
	}

	public void setGuest(Guest guest) {
		this.guest = guest;
	}

	public Bill getBill() {
		return bill;
	}

	public void setBill(Bill bill) {
		this.bill = bill;
	}

}
