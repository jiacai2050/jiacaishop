package com.jiacai.pagination;

import java.util.List;

import com.jiacai.bean.GuestBill;

public class GuestBillPage extends PageEntity {

	private List<GuestBill> guestBills;

	public GuestBillPage() {
	}

	public GuestBillPage(int currentPage, int numberPerPage, int sumPage,String pageNavi,
			List<GuestBill> guestBills) {
		super(currentPage, numberPerPage, sumPage, pageNavi);
		this.guestBills = guestBills;

	}

	public List<GuestBill> getGuestBills() {
		return guestBills;
	}

	public void setGuestBills(List<GuestBill> guestBills) {
		this.guestBills = guestBills;
	}

}
