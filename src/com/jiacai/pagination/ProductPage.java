package com.jiacai.pagination;

import java.util.List;

import com.jiacai.bean.Product;


public class ProductPage extends PageEntity {

	private List<Product> products;

	public ProductPage(int currentPage, int numberPerPage, int sumPage,String pageNavi,
			List<Product> products) {
		super(currentPage, numberPerPage, sumPage, pageNavi);
		this.products = products;

	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}
	
	

}
