package com.jiacai.pagination;

import java.util.List;

import com.jiacai.bean.Guest;

public class GuestPage extends PageEntity{

	private List<Guest> guests;

	public GuestPage(int currentPage, int numberPerPage, int sumPage,String pageNavi,List<Guest> guests) {
		super(currentPage, numberPerPage, sumPage, pageNavi);
		this.guests = guests;
	}

	public List<Guest> getGuests() {
		return guests;
	}

	public void setGuests(List<Guest> guests) {
		this.guests = guests;
	}
 
}
