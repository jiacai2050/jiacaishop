package com.jiacai.pagination;

public abstract class PageEntity {
	
	private int currentPage;
	private int numberPerPage;
	private int sumPage;
	private String pageNavi;
	
	public PageEntity() {
		super();
	}

	public PageEntity(int currentPage, int numberPerPage, int sumPage, String pageNavi) {
		this.currentPage = currentPage;
		this.numberPerPage = numberPerPage;
		this.sumPage = sumPage;
		this.pageNavi = pageNavi;
	}
	
	public int getCurrentPage() {
		return currentPage;
	}


	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}


	public int getNumberPerPage() {
		return numberPerPage;
	}


	public void setNumberPerPage(int numberPerPage) {
		this.numberPerPage = numberPerPage;
	}


	public int getSumPage() {
		return sumPage;
	}


	public void setSumPage(int sumPage) {
		this.sumPage = sumPage;
	}

	public String getPageNavi() {
		return pageNavi;
	}

	public void setPageNavi(String pageNavi) {
		this.pageNavi = pageNavi;
	}
}
