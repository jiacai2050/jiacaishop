package com.jiacai.pagination;

import java.util.List;

import com.jiacai.bean.Import;
import com.jiacai.bean.Product;


public class ImportPage extends PageEntity {

	private List<Import> imports;

	public ImportPage(int currentPage, int numberPerPage, int sumPage,String pageNavi,
			List<Import> imports) {
		super(currentPage, numberPerPage, sumPage, pageNavi);
		this.imports = imports;

	}

	public List<Import> getImports() {
		return imports;
	}

	public void setImports(List<Import> imports) {
		this.imports = imports;
	}
}
