package com.jiacai.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.jiacai.bean.Commodity;
import com.jiacai.bean.Guest;
import com.jiacai.bean.Product;
import com.jiacai.util.Table;

public class CommodityDAO extends BaseDAO {

	public boolean add(Commodity commodity) {
		boolean res = false;

		int fid = commodity.getFid();
		int kind = commodity.getKind();
		int pid = commodity.getPid();
		int number = commodity.getNumber();
		double unitprice = commodity.getUnitprice();
		double sumprice = commodity.getSumprice();
		
		String sql = "insert into " + Table.COMMODITY
				+ "  (fid,kind,pid,number,unitprice,sumprice)  values (?, ?, ?, ?, ?, ?)";

		try (PreparedStatement prestat = conn.prepareStatement(sql)) {

			prestat.setInt(1, fid);;
			prestat.setInt(2, kind);
			prestat.setInt(3, pid);
			prestat.setInt(4, number);
			prestat.setDouble(5, unitprice);
			prestat.setDouble(6, sumprice);
			
			res = prestat.executeUpdate() > 0;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}
	
	@Override
	public List<Commodity> executeQuery(String sql) {
		try (Statement stat = conn.createStatement()) {
			ResultSet rs = stat.executeQuery(sql);
			List<Commodity> commodities = new ArrayList<Commodity>();
			while (rs.next()) {
				int id = rs.getInt("id");
				int fid = rs.getInt("fid");
				int kind = rs.getInt("kind");
				int pid = rs.getInt("pid");
				int number = rs.getInt("number");
				double unitprice = rs.getDouble("unitprice");
				double sumprice = rs.getDouble("sumprice");
				
				commodities.add(new Commodity(id, fid, kind, pid, number, unitprice, sumprice));
			}
			return commodities;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<Commodity> select(int fid,int kind) {
		String sql = "select id,pid,number,unitprice,sumprice from " + Table.COMMODITY
				+ " where fid=? and kind=?";
		try (PreparedStatement prestat = conn.prepareStatement(sql);) {
			List<Commodity> commodities = new ArrayList<>();
			prestat.setInt(1, fid);
			prestat.setInt(2, kind);
			ResultSet rs = prestat.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				int pid = rs.getInt("pid");
				int number = rs.getInt("number");
				double unitprice = rs.getDouble("unitprice");
				double sumprice = rs.getDouble("sumprice");
				commodities.add(new Commodity(id, fid, kind, pid, number, unitprice, sumprice));
			}
			return commodities;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	} 
	
	@Override
	public Commodity select(int id) {
		Commodity commodity = null;
		String sql = "select fid,kind,pid,number,unitprice,sumprice from " + Table.COMMODITY
				+ " where id=?";

		try (PreparedStatement prestat = conn.prepareStatement(sql);) {

			prestat.setInt(1, id);
			ResultSet rs = prestat.executeQuery();
			if (rs.next()) {
				int fid = rs.getInt("fid");
				int kind = rs.getInt("kind");
				int pid = rs.getInt("pid");
				int number = rs.getInt("number");
				double unitprice = rs.getDouble("unitprice");
				double sumprice = rs.getDouble("sumprice");
				
				commodity = new Commodity(id, fid, kind, pid, number, unitprice, sumprice);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return commodity;


	}

	@Override
	public boolean deleteById(int id) {
		boolean res = false;
		String sql = "delete from " + Table.COMMODITY + " where id=?";

		try (PreparedStatement prestat = conn.prepareStatement(sql)) {
			prestat.setInt(1, id);
			res = prestat.executeUpdate() > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}
	
	public boolean delete(int fid,int kind,int pid) {
		boolean res = false;
		String sql = "delete from " + Table.COMMODITY + " where fid=? and kind=? and pid=?";

		try (PreparedStatement prestat = conn.prepareStatement(sql)) {
			prestat.setInt(1, fid);
			prestat.setInt(2, kind);
			prestat.setInt(3, pid);
			res = prestat.executeUpdate() > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public boolean update(Commodity commodity) {
		boolean res = false;
		int id = commodity.getId();
		int fid = commodity.getFid();
		int kind = commodity.getKind();
		int pid = commodity.getPid();
		int number = commodity.getNumber();
		double unitprice = commodity.getUnitprice();
		double sumprice = commodity.getSumprice();
		 
		String sql = "update " + Table.COMMODITY
				+ " set fid=?,kind=?,pid=?,number=?,unitprice=?,sumprice=? where id=?";

		try (PreparedStatement prestat = conn.prepareStatement(sql)) {
			int idx = 1;
			prestat.setInt(idx++, fid);
			prestat.setInt(idx++, kind);
			prestat.setInt(idx++, pid);
			prestat.setInt(idx++, number);
			prestat.setDouble(idx++, unitprice);
			prestat.setDouble(idx++, sumprice);
			prestat.setInt(idx++, id);
			
			res = prestat.executeUpdate() == 1;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}
	 
}
