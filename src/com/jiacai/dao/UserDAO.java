package com.jiacai.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.jiacai.bean.Product;
import com.jiacai.bean.User;
import com.jiacai.util.Table;

public class UserDAO extends BaseDAO {

	public boolean add(User user) {

		boolean res = false;

		int role = user.getRole();
		String name = user.getName();
		String pwd = user.getPwd();

		String sql = "insert into " + Table.USER
				+ "  (role, name,pwd)  values (?, ?, ?)";

		try (PreparedStatement prestat = conn.prepareStatement(sql)) {
			prestat.setInt(1, role);
			prestat.setString(2, name);
			prestat.setString(3, pwd);
			res = prestat.executeUpdate() > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public User select(String name) {
		String sql = "select * from " + Table.USER + " where name=?";

		try (PreparedStatement prestat = conn.prepareStatement(sql)) {
			prestat.setString(1, name);
			ResultSet rs = prestat.executeQuery();
			while (rs.next()) {
				return new User(rs.getInt("id"), rs.getInt("role"), rs.getString("name"),
						rs.getString("pwd"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;

	}

	@Override
	public List<User> executeQuery(String sql) {
		try (Statement stat = conn.createStatement()) {
			ResultSet rs = stat.executeQuery(sql);
			List<User> users = new ArrayList<User>();
			while (rs.next()) {
				int id = rs.getInt("id");
				int role = rs.getInt("role");
				String name = rs.getString("name");
				String pwd = rs.getString("pwd");
				users.add(new User(id, role, name, pwd));
			}
			return users;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public User select(int id) {
		User user = null;
		String sql = "select role, name, pwd from " + Table.USER
				+ " where id=?";

		try (PreparedStatement prestat = conn.prepareStatement(sql);) {

			prestat.setInt(1, id);
			ResultSet rs = prestat.executeQuery();
			if (rs.next()) {

				int role = rs.getInt("role");
				String name = rs.getString("name");
				String pwd = rs.getString("pwd");

				user = new User(id, role, name, pwd);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}
	
	@Override
	public boolean deleteById(int id) {
		boolean res = false;
		String sql = "delete from " + Table.USER + " where id=?";

		try (PreparedStatement prestat = conn.prepareStatement(sql)) {
			prestat.setInt(1, id);
			res = prestat.executeUpdate() > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

}
