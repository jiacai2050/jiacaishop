package com.jiacai.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.jiacai.bean.Price;
import com.jiacai.util.Table;

public class PriceDAO extends BaseDAO {

	public boolean update(Price price) {
		int gid = price.getGid();
		int pid = price.getPid();
		double unitprice = price.getUnitprice();
		boolean res = false;

		String sql = "SELECT EXISTS(select * from " + Table.PRICE
				+ " where gid=? and pid=?)";

		// String sql = "update " + Table.PRODUCT
		// + " set name=?, specification=? where id=?";

		try (PreparedStatement prestat = conn.prepareStatement(sql)) {
			prestat.setInt(1, gid);
			prestat.setInt(2, pid);
			ResultSet rs = prestat.executeQuery();
			rs.absolute(1);
			Statement stat = conn.createStatement();
			if (1 == rs.getInt(1)) { // 以前有
				stat.executeUpdate("update " + Table.PRICE + " set unitprice="
						+ unitprice + " where gid=" + gid + " and pid=" + pid);
			} else {
				stat.execute("insert into " + Table.PRICE
						+ "(gid,pid,unitprice) values(" + gid + "," + pid + ","
						+ unitprice + ")");
			}
			stat.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	@Override
	public Object executeQuery(String sql) {
		return null;
	}

	@Override
	public Object select(int id) {
		throw new UnsupportedOperationException(Table.PRICE + "主键不唯一");
	}

	public Price select(int gid, int pid) {
		Price price = null;
		String sql = "select unitprice from " + Table.PRICE
				+ " where gid=? and pid=?";
		try (PreparedStatement prestat = conn.prepareStatement(sql)) {
			prestat.setInt(1, gid);
			prestat.setInt(2, pid);
			ResultSet rs = prestat.executeQuery();
			if (rs.next()) {
				price = new Price(gid, pid, rs.getDouble("unitprice"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return price;
	}
	@Override
	public boolean deleteById(int id) {
		throw new UnsupportedOperationException(Table.PRICE + "主键不唯一");
	}

}
