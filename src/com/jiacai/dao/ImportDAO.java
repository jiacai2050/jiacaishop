package com.jiacai.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.jiacai.bean.Import;
import com.jiacai.util.Table;


public class ImportDAO extends BaseDAO{

	public boolean add(Import im) {
		boolean res = false;

		int uid = im.getUid();
		Date date = im.getDate();
		String supplier = im.getSupplier();
		double sumprice = im.getSumprice();

		String sql = "insert into " + Table.IMPORT
				+ "  (uid, date, supplier, sumprice)  values (?, ?, ?, ?)";

		try (PreparedStatement prestat = conn.prepareStatement(sql)) {

			prestat.setInt(1, uid);
			prestat.setDate(2, new java.sql.Date(date.getTime()));
			prestat.setString(3, supplier);
			prestat.setDouble(4, sumprice);
			
			res = prestat.executeUpdate() > 0;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}
	
	@Override
	public List<Import> executeQuery(String sql) {
		try (Statement stat = conn.createStatement()) {
			ResultSet rs = stat.executeQuery(sql);
			List<Import> imports = new ArrayList<Import>();
			while (rs.next()) {
				int id = rs.getInt("id");
				int uid = rs.getInt("uid");
				Date date = rs.getDate("date");
				String supplier = rs.getString("supplier");
				double sumprice = rs.getDouble("sumprice");
				imports.add(new Import(id, uid, date, supplier, sumprice));
			}
			return imports;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Import select(int id) {
		Import im = null;
		String sql = "select uid, date,supplier,sumprice from " + Table.IMPORT
				+ " where id=?";

		try (PreparedStatement prestat = conn.prepareStatement(sql);) {

			prestat.setInt(1, id);
			ResultSet rs = prestat.executeQuery();
			if (rs.next()) {
				int uid = rs.getInt("uid");
				Date date = rs.getDate("date");
				String supplier = rs.getString("supplier");
				double sumprice = rs.getDouble("sumprice");

				im = new Import(id, uid, date, supplier, sumprice);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return im;

	}

	@Override
	public boolean deleteById(int id) {
		boolean res = false;
		String sql = "delete from " + Table.IMPORT + " where id=?";

		try (PreparedStatement prestat = conn.prepareStatement(sql)) {
			prestat.setInt(1, id);
			res = prestat.executeUpdate() > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}
	
	public boolean update(Import im) {
		boolean res = false;
		int id = im.getId();
		int uid = im.getUid();
		Date date = im.getDate();
		String supplier = im.getSupplier();
		double sumprice = im.getSumprice();
		 
		String sql = "update " + Table.IMPORT
				+ " set uid=?,date=?,supplier=?,sumprice=? where id=?";

		try (PreparedStatement prestat = conn.prepareStatement(sql)) {
			prestat.setInt(1, uid);
			prestat.setDate(2, new java.sql.Date(date.getTime()));
			prestat.setString(3, supplier);
			prestat.setDouble(4, sumprice);
			prestat.setInt(5, id);
			
			res = prestat.executeUpdate() == 1;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

}
