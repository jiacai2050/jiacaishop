package com.jiacai.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.jiacai.bean.Warehouse;
import com.jiacai.util.Table;

public class WarehouseDAO extends BaseDAO {

	public boolean add(Warehouse warehouse) {
		String sql = "insert into " + Table.WAREHOUSE
				+ "  (pid, number)  values (?, ?)";

		try (PreparedStatement stat = conn.prepareStatement(sql)) {
			stat.setInt(1, warehouse.getPid());
			stat.setLong(2, warehouse.getNumber());

			return stat.executeUpdate() == 1;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean update(Warehouse warehouse) {

		String sql = "update " + Table.WAREHOUSE + " set number=? where id=?";
		try (PreparedStatement prestat = conn.prepareStatement(sql)) {
			prestat.setLong(1, warehouse.getNumber());
			prestat.setInt(2, warehouse.getPid());

			return prestat.executeUpdate() == 1;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public List<Warehouse> executeQuery(String sql) {

		try (Statement stat = conn.createStatement()) {
			ResultSet rs = stat.executeQuery(sql);
			List<Warehouse> wares = new ArrayList<Warehouse>();
			while (rs.next()) {
				int pid = rs.getInt("pid");
				long number = rs.getLong("number");
				wares.add(new Warehouse(pid, number));
			}
			return wares;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Warehouse select(int id) {
		Warehouse warehouse = null;
		String sql = "select number from " + Table.WAREHOUSE + " where pid=?";

		try (PreparedStatement prestat = conn.prepareStatement(sql);) {

			prestat.setInt(1, id);
			ResultSet rs = prestat.executeQuery();
			if (rs.next()) {
				long number = rs.getLong("number");

				warehouse = new Warehouse(id, number);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return warehouse;
	}

	@Override
	public boolean deleteById(int id) {
		boolean res = false;
		String sql = "delete from " + Table.WAREHOUSE + " where pid=?";

		try (PreparedStatement prestat = conn.prepareStatement(sql)) {
			prestat.setInt(1, id);
			res = prestat.executeUpdate() > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

}
