package com.jiacai.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.jiacai.bean.Product;
import com.jiacai.util.Table;

public class ProductDAO extends BaseDAO {

	public boolean add(Product product) {
		boolean res = false;

		String name = product.getName();
		String specification = product.getSpecification();

		String sql = "insert into " + Table.PRODUCT
				+ "  (name,specification)  values (?, ?)";

		try (PreparedStatement prestat = conn.prepareStatement(sql)) {

			prestat.setString(1, name);
			prestat.setString(2, specification);

			res = prestat.executeUpdate() > 0;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public int deleteByIds(int[] ids) {

		int res = -1;
		StringBuilder sql = new StringBuilder("delete from " + Table.PRODUCT
				+ " where id in (");
		for (int i = 0; i < ids.length - 1; i++) {
			sql.append(ids[i] + ",");
		}
		if (ids.length > 0) {
			sql.append(ids[ids.length - 1] + ")");
		}

		try (Statement stat = conn.createStatement()) {

			res = stat.executeUpdate(sql.toString());

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public boolean update(Product product) {
		boolean res = false;
		int id = product.getId();
		String name = product.getName();
		String specification = product.getSpecification();
		String sql = "update " + Table.PRODUCT
				+ " set name=?, specification=? where id=?";

		try (PreparedStatement prestat = conn.prepareStatement(sql)) {
			prestat.setString(1, name);
			prestat.setString(2, specification);
			prestat.setInt(3, id);

			res = prestat.executeUpdate() == 1;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public List<Product> select(String name) {
		List<Product> products = new ArrayList<Product>();

		String sql = "select id, name, specification from " + Table.PRODUCT
				+ " where name like ?";

		try (PreparedStatement prestat = conn.prepareStatement(sql)) {
			prestat.setString(1, "%" + name + "%");
			ResultSet rs = prestat.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				String pname = rs.getString("name");
				String specification = rs.getString("specification");
				products.add(new Product(id, pname, specification));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return products;
	}

	@Override
	public Product select(int id) {
		Product product = null;
		String sql = "select name, specification  from " + Table.PRODUCT
				+ " where id=?";

		try (PreparedStatement prestat = conn.prepareStatement(sql);) {

			prestat.setInt(1, id);
			ResultSet rs = prestat.executeQuery();
			if (rs.next()) {

				String name = rs.getString("name");
				String specification = rs.getString("specification");

				product = new Product(id, name, specification);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return product;

	}

	@Override
	public List<Product> executeQuery(String sql) {

		try (Statement stat = conn.createStatement()) {
			ResultSet rs = stat.executeQuery(sql);
			List<Product> products = new ArrayList<Product>();
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String specification = rs.getString("specification");
				products.add(new Product(id, name, specification));
			}
			return products;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean deleteById(int id) {
		boolean res = false;
		String sql = "delete from " + Table.PRODUCT + " where id=?";

		try (PreparedStatement prestat = conn.prepareStatement(sql)) {
			prestat.setInt(1, id);
			res = prestat.executeUpdate() > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

}
