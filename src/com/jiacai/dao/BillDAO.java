package com.jiacai.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.jiacai.bean.Bill;
import com.jiacai.bean.Import;
import com.jiacai.util.Table;


public class BillDAO extends BaseDAO{

	public boolean add(Bill bill) {
		boolean res = false;

		int gid = bill.getGid();
		Date date = bill.getDate();
		double sumprice = bill.getSumprice();

		String sql = "insert into " + Table.BILL
				+ "  (gid, date, sumprice)  values (?, ?, ?)";

		try (PreparedStatement prestat = conn.prepareStatement(sql)) {

			prestat.setInt(1, gid);
			prestat.setDate(2, new java.sql.Date(date.getTime()));
			prestat.setDouble(3, sumprice);
			
			res = prestat.executeUpdate() > 0;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}
	
	@Override
	public List<Bill> executeQuery(String sql) {
		try (Statement stat = conn.createStatement()) {
			ResultSet rs = stat.executeQuery(sql);
			List<Bill> bills = new ArrayList<Bill>();
			while (rs.next()) {
				int id = rs.getInt("id");
				int gid = rs.getInt("gid");
				Date date = rs.getDate("date");
				double sumprice = rs.getDouble("sumprice");
				bills.add(new Bill(id, gid, date,sumprice));
			}
			return bills;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Bill select(int id) {
		Bill bill = null;
		String sql = "select gid, date,sumprice from " + Table.BILL
				+ " where id=?";

		try (PreparedStatement prestat = conn.prepareStatement(sql);) {

			prestat.setInt(1, id);
			ResultSet rs = prestat.executeQuery();
			if (rs.next()) {
				int gid = rs.getInt("gid");
				Date date = rs.getDate("date");
				double sumprice = rs.getDouble("sumprice");
				bill = new Bill(id, gid, date, sumprice);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return bill;

	}

	@Override
	public boolean deleteById(int id) {
		boolean res = false;
		String sql = "delete from " + Table.BILL + " where id=?";

		try (PreparedStatement prestat = conn.prepareStatement(sql)) {
			prestat.setInt(1, id);
			res = prestat.executeUpdate() > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}
	
	public boolean update(Bill bill) {
		boolean res = false;
		int id = bill.getId();
		int gid = bill.getGid();
		Date date = bill.getDate();
		double sumprice = bill.getSumprice();
		 
		String sql = "update " + Table.BILL
				+ " set gid=?,date=?,sumprice=? where id=?";

		try (PreparedStatement prestat = conn.prepareStatement(sql)) {
			prestat.setInt(1, gid);
			prestat.setDate(2, new java.sql.Date(date.getTime()));
			prestat.setDouble(3, sumprice);
			prestat.setInt(4, id);
			
			res = prestat.executeUpdate() == 1;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

}
