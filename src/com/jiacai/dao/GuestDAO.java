package com.jiacai.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.jiacai.bean.Guest;
import com.jiacai.bean.User;
import com.jiacai.util.Table;

public class GuestDAO extends BaseDAO {

	public boolean add(Guest guest) {

		boolean res = false;

		String name = guest.getName();
		String address = guest.getAddress();
		String tel = guest.getTel();

		String sql = "insert into " + Table.GUEST
				+ "  (name,address,tel)  values (?, ?, ?)";

		try (PreparedStatement prestat = conn.prepareStatement(sql)) {
			prestat.setString(1, name);
			prestat.setString(2, address);
			prestat.setString(3, tel);
			res = prestat.executeUpdate() > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public List<Guest> selectByName(String name) {

		String sql = "select * from " + Table.USER + " where name=?";

		List<Guest> guests = new ArrayList<Guest>();
		try (PreparedStatement prestat = conn.prepareStatement(sql)) {
			prestat.setString(1, name);
			ResultSet rs = prestat.executeQuery();
			while (rs.next()) {
				guests.add(new Guest(rs.getInt("id"), rs.getString("name"), rs
						.getString("address"), rs.getString("tel")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return guests;
	}

	public boolean update(Guest guest) {
		String sql = "update " + Table.GUEST
				+ " set name=?, address=?, tel=? where id=?";
		try (PreparedStatement prestat = conn.prepareStatement(sql)) {
			prestat.setString(1, guest.getName());
			prestat.setString(2, guest.getAddress());
			prestat.setString(3, guest.getTel());
			prestat.setInt(4, guest.getId());

			return prestat.executeUpdate() == 1;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public List<Guest> executeQuery(String sql) {
		try (Statement stat = conn.createStatement()) {
			ResultSet rs = stat.executeQuery(sql);
			List<Guest> guests = new ArrayList<Guest>();
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String address = rs.getString("address");
				String tel = rs.getString("tel");
				guests.add(new Guest(id, name,address,tel));
			}
			return guests;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public Guest select(int id) {
		Guest guest = null;
		String sql = "select name,address,tel from " + Table.GUEST
				+ " where id=?";

		try (PreparedStatement prestat = conn.prepareStatement(sql);) {

			prestat.setInt(1, id);
			ResultSet rs = prestat.executeQuery();
			if (rs.next()) {

				String name = rs.getString("name");
				String address = rs.getString("address");
				String tel = rs.getString("tel");

				guest = new Guest(id, name, address, tel);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return guest;

	}
	
	@Override
	public boolean deleteById(int id) {
		boolean res = false;
		String sql = "delete from " + Table.GUEST + " where id=?";

		try (PreparedStatement prestat = conn.prepareStatement(sql)) {
			prestat.setInt(1, id);
			res = prestat.executeUpdate() > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

}
