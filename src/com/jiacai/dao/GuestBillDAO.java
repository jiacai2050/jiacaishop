package com.jiacai.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.jiacai.bean.Bill;
import com.jiacai.bean.Guest;
import com.jiacai.bean.GuestBill;

public class GuestBillDAO extends BaseDAO{

	@Override
	public List<GuestBill> executeQuery(String sql) {
		try (Statement stat = conn.createStatement()) {
			ResultSet rs = stat.executeQuery(sql);
			List<GuestBill> guestBills = new ArrayList<GuestBill>();
			while (rs.next()) {
				int id = rs.getInt("id");
				int gid = rs.getInt("gid");
				String name = rs.getString("name");
				String address = rs.getString("address");
				String tel = rs.getString("tel");
				Date date = rs.getDate("date");
				double sum = rs.getDouble("sumprice");
				guestBills.add(new GuestBill(new Guest(gid,name, address, tel), new Bill(id, gid, date, sum)));
			}
			return guestBills;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Object select(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deleteById(int id) {
		// TODO Auto-generated method stub
		return false;
	}

}
