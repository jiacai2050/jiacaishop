package com.jiacai.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.jiacai.util.DBHelper;
import com.jiacai.util.Table;

public abstract class BaseDAO {

	Connection conn = DBHelper.getConnection();

	public int getTotalRow() {
		try {
			ResultSet rs = conn.createStatement().executeQuery(
					"select FOUND_ROWS()");
			rs.absolute(1);
			return rs.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public int getLastInsertID(Table table) {
		int res = -1;

		try (Statement stat = conn.createStatement()) {
			ResultSet rs = stat.executeQuery("select max(id) from " + table);
			if (rs.next()) {
				res = rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	public void setTransactionOn() {
		try {
			conn.setAutoCommit(false);
		} catch (SQLException e) {
			System.out.println("事务开始失败");
			e.printStackTrace();
		}
	}

	public void submitTransaction() {
		try {
			conn.commit();
			conn.setAutoCommit(true);
		} catch (SQLException e) {
			System.out.println("事务提交失败");
			e.printStackTrace();
		}
	}

	public void rollbackTransaction() {
		try {
			conn.rollback();
			conn.setAutoCommit(true);
		} catch (SQLException e) {
			System.out.println("事务回滚失败");
			e.printStackTrace();
		}
	}
	
	public abstract Object executeQuery(String sql);
	public abstract Object select(int id);
	public abstract boolean deleteById(int id);
		
}
