package com.jiacai.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.jiacai.bean.WayBillPageDiv;
import com.jiacai.bean.Waybill;
import com.jiacai.util.DBHelper;
import com.jiacai.util.Table;

public class WaybillDAO {

	Connection conn = DBHelper.getConnection();

	public boolean add(Waybill waybill) {
		boolean res = false;

		String ordername = waybill.getOrdername();
		String phone = waybill.getPhone();
		String address = waybill.getAddress();
		int pnum = waybill.getPnum();
		double sum = waybill.getSum();
		Date pdate = waybill.getPdate();

		String sql = "insert into " + Table.WAYBILL
				+ " values (null, ?, ?, ?, ?, ?, ?)";

		PreparedStatement prestat = null;
		try {
			prestat = conn.prepareStatement(sql);
			prestat.setString(1, ordername);
			prestat.setString(2, phone);
			prestat.setString(3, address);
			prestat.setInt(4, pnum);
			prestat.setDouble(5, sum);
			prestat.setDate(6, new java.sql.Date(pdate.getTime()));

			res = prestat.executeUpdate() > 0;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (null != prestat)
				try {
					prestat.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		return res;
	}

	public boolean deleteById(int id) {
		boolean res = false;
		String sql = "delete from " + Table.WAYBILL + " where id=?";
		PreparedStatement prestat = null;
		try {
			prestat = conn.prepareStatement(sql);
			prestat.setInt(1, id);

			res = prestat.executeUpdate() > 0;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (null != prestat)
				try {
					prestat.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		return res;
	}

	public int deleteByIds(int[] ids) {

		int res = -1;
		StringBuilder sql = new StringBuilder("delete from " + Table.WAYBILL
				+ " where id in (");
		for (int i = 0; i < ids.length - 1; i++) {
			sql.append(ids[i] + ",");
		}
		if (ids.length > 0) {
			sql.append(ids[ids.length - 1] + ")");
		}
		Statement stat = null;
		try {
			stat = conn.createStatement();
			res = stat.executeUpdate(sql.toString());

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (null != stat)
				try {
					stat.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		return res;
	}

	public boolean update(Waybill waybill) {
		boolean res = false;
		int id = waybill.getId();
		String ordername = waybill.getOrdername();
		String phone = waybill.getPhone();
		String address = waybill.getAddress();
		int pnum = waybill.getPnum();
		double sum = waybill.getSum();
		Date pdate = waybill.getPdate();

		String sql = "update "
				+ Table.WAYBILL
				+ " set ordername=?, phone=?, address=?, pnum=?, sum=?, pdate=? where id=?";

		PreparedStatement prestat = null;
		try {
			prestat = conn.prepareStatement(sql);
			prestat.setString(1, ordername);
			prestat.setString(2, phone);
			prestat.setString(3, address);
			prestat.setInt(4, pnum);
			prestat.setDouble(5, sum);
			prestat.setDate(6, new java.sql.Date(pdate.getTime()));
			prestat.setInt(7, id);

			res = prestat.executeUpdate() > 0;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (null != prestat)
				try {
					prestat.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		return res;
	}

	public List<Waybill> select(String sql) {
		List<Waybill> waybills = new ArrayList<Waybill>();
		PreparedStatement prestat = null;
		try {
			prestat = conn.prepareStatement(sql);
			ResultSet rs = prestat.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				String ordername = rs.getString("ordername");
				String phone = rs.getString("phone");
				String address = rs.getString("address");
				int pnum = rs.getInt("pnum");
				double sumprice = rs.getDouble("sum");
				Date pdate = new Date(rs.getDate("pdate").getTime());
				waybills.add(new Waybill(id, ordername, phone, address, pnum,
						sumprice, pdate));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (null != prestat)
				try {
					prestat.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		return waybills;
	}

	public Waybill selectById(int id) {
		PreparedStatement prestat = null;
		String sql = "select * from " + Table.WAYBILL + " where id=?";
		try {
			prestat = conn.prepareCall(sql);
			prestat.setInt(1, id);
			ResultSet rs = prestat.executeQuery();
			while (rs.next()) {
				String ordername = rs.getString("ordername");
				String phone = rs.getString("phone");
				String address = rs.getString("address");
				int pnum = rs.getInt("pnum");
				double sumprice = rs.getDouble("sum");
				Date pdate = new Date(rs.getDate("pdate").getTime());
				return new Waybill(id, ordername, phone, address, pnum,
						sumprice, pdate);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (null != prestat)
				try {
					prestat.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		return null;
	}

	public WayBillPageDiv select4PageDiv(String sql, int pageNo) {

		Statement stat = null;
		List<Waybill> waybills = null;
		int total = 0;
		int pageNumber = 10;
		boolean hasResult = false;
		try {
			stat = conn.createStatement();
			ResultSet rs = stat.executeQuery(sql + " limit " + (pageNo - 1)
					* pageNumber + "," + pageNumber);
			waybills = new ArrayList<Waybill>();
			while (rs.next()) {
				int id = rs.getInt("id");
				String ordername = rs.getString("ordername");
				String phone = rs.getString("phone");
				String address = rs.getString("address");
				int pnum = rs.getInt("pnum");
				double sumprice = rs.getDouble("sum");
				Date pdate = new Date(rs.getDate("pdate").getTime());
				waybills.add(new Waybill(id, ordername, phone, address, pnum,
						sumprice, pdate));
			}
			ResultSet rs4total = stat.executeQuery("select FOUND_ROWS()");
			rs4total.absolute(1);
			total = rs4total.getInt(1);
			hasResult = total > 0;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (null != stat)
				try {
					stat.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		StringBuilder sb = new StringBuilder();
		int sum = 0;
		if (hasResult) {
			
		}
		return new WayBillPageDiv(waybills, sb.toString(), pageNo, sum);
	}

}
