package com.jiacai.servlet;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jiacai.bean.Price;
import com.jiacai.dao.PriceDAO;
import com.jiacai.util.StringUtil;

@WebServlet("/PriceAjax")
public class PriceAjax extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		int pid = StringUtil.str2int(request.getParameter("pid"));
		String gid = request.getParameter("gid");
		if (StringUtil.nullOrEmpty(gid)) {
			out.write(0);
		} else {
			PriceDAO dao = new PriceDAO();
			Price price = dao.select(StringUtil.str2int(gid), pid);
			if (null == price) {
				out.write(0);
			} else {
				out.print(price.getUnitprice());
			}
		}
		out.flush();
		out.close();
	}

}
