package com.jiacai.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jiacai.bean.Waybill;
import com.jiacai.dao.WaybillDAO;
import com.jiacai.util.DBHelper;

@WebServlet("/FindPhoneAjaxServlet")
public class FindPhoneAjaxServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doPost(request, response);
	}
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		WaybillDAO dao = new WaybillDAO();
		String addr = request.getParameter("addr");
		String order = request.getParameter("order");
		String sql = "select * from waybill where address='"+addr+"' && ordername='"+order+"'";
		List<Waybill> waybills = dao.select(sql);
		String res = "";
		if(waybills.size()>0) {
			res = waybills.get(0).getPhone();
		}
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		out.print(res);
		out.close();
		
	}

}
