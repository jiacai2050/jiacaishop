package com.jiacai.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jiacai.pagination.GuestBillPage;
import com.jiacai.service.GuestBillService;

@WebServlet("/SearchBill")
public class SearchBill extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		GuestBillService service = new GuestBillService();

		GuestBillPage page = service.genPagination(request, 3);

		request.setAttribute("result", page);
		
		request.getRequestDispatcher("bill/search.jsp").forward(request, response);

	}
}
