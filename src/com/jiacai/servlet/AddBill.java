package com.jiacai.servlet;

import java.io.IOException;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jiacai.bean.Bill;
import com.jiacai.bean.Commodity;
import com.jiacai.bean.Price;
import com.jiacai.dao.BillDAO;
import com.jiacai.dao.CommodityDAO;
import com.jiacai.dao.PriceDAO;
import com.jiacai.util.StringUtil;
import com.jiacai.util.Table;

@WebServlet("/AddBill")
public class AddBill extends HttpServlet {

	public AddBill() {
		super();
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			String date = request.getParameter("date");
			int gid = StringUtil.str2int(request.getParameter("gid"));
			String pnumber = request.getParameter("pnumber");
			double sum = StringUtil.str2double(request.getParameter("sum"));

			Bill bill = new Bill(gid, StringUtil.str2date(date), sum);

			BillDAO dao = new BillDAO();
			if (dao.add(bill)) {
				int lastid = dao.getLastInsertID(Table.BILL);
				int total = 0;
				if (pnumber != null && pnumber.trim() != "") {
					Pattern p = Pattern.compile("[0-9]*");
					if (p.matcher(pnumber).matches()) {
						total = Integer.valueOf(pnumber);
					}
				}
				if (total > 0) {
					CommodityDAO cdao = new CommodityDAO();
					PriceDAO pdao = new PriceDAO();
					for (int i = 1; i <= total; i++) {
						int pid = StringUtil.str2int(request.getParameter("pid"
								+ i));
						int number = StringUtil.str2int(request
								.getParameter("number" + i));
						double unitprice = StringUtil.str2double(request
								.getParameter("unitprice" + i));
						double sumprice = StringUtil.str2double(request
								.getParameter("sumprice" + i));

						cdao.add(new Commodity(lastid, 2, pid, number,
								unitprice, sumprice));
						
						pdao.update(new Price(gid, pid, unitprice));
						
					}
					response.sendRedirect("bill/result.jsp?res=1&from=0&id="+ lastid);
				} else {
					response.sendRedirect("bill/result.jsp?res=0&from=0");
				}

			}
		} catch (Exception e) {
			//res=1表示成功，from=0表示从添加回到result.jsp页面，与修改时回到result.jsp区分
			response.sendRedirect("bill/result.jsp?res=0&from=0");
		}
	}

}
