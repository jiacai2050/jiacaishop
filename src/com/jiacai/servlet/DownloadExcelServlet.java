package com.jiacai.servlet;

import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jiacai.dao.WaybillDAO;
import com.jiacai.util.ExcelHelper;
import com.jiacai.util.StringUtil;

@WebServlet("/DownloadExcelServlet")
public class DownloadExcelServlet extends HttpServlet {
	public DownloadExcelServlet() {
		super();
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		WaybillDAO dao = new WaybillDAO();
		Date now = Calendar.getInstance().getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss");
		String fname = sdf.format(now) + ".xls";
        response.setCharacterEncoding("UTF-8");  
        fname = java.net.URLEncoder.encode(fname,"UTF-8");  
        
        response.setHeader("Content-Disposition","attachment;filename="+fname);  
        response.setContentType("application/msexcel");  

        OutputStream os = response.getOutputStream();
        ExcelHelper.write(dao.select(StringUtil.querySQL), os);
        
        os.flush();
        os.close();
        
	}

}
