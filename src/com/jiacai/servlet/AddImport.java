package com.jiacai.servlet;

import java.io.IOException;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jiacai.bean.Commodity;
import com.jiacai.bean.Import;
import com.jiacai.bean.Product;
import com.jiacai.bean.Waybill;
import com.jiacai.dao.CommodityDAO;
import com.jiacai.dao.ImportDAO;
import com.jiacai.dao.ProductDAO;
import com.jiacai.dao.WaybillDAO;
import com.jiacai.util.DBHelper;
import com.jiacai.util.StringUtil;
import com.jiacai.util.Table;

@WebServlet("/AddImport")
public class AddImport extends HttpServlet {

	public AddImport() {
		super();
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			String date = request.getParameter("date");
			String supplier = request.getParameter("supplier");
			String pnumber = request.getParameter("pnumber");
			double sum = StringUtil.str2double(request.getParameter("sum"));

			int uid = StringUtil.str2int(request.getSession()
					.getAttribute("uid").toString());

			Import im = new Import(uid, StringUtil.str2date(date), supplier,
					sum);

			ImportDAO dao = new ImportDAO();
			if (dao.add(im)) {
				int lastid = dao.getLastInsertID(Table.IMPORT);
				int total = 0;
				if (pnumber != null && pnumber.trim() != "") {
					Pattern p = Pattern.compile("[0-9]*");
					if (p.matcher(pnumber).matches()) {
						total = Integer.valueOf(pnumber);
					}
				}
				if (total > 0) {
					CommodityDAO cdao = new CommodityDAO();
					for (int i = 1; i <= total; i++) {
						int pid = StringUtil.str2int(request.getParameter("pid"
								+ i));
						int number = StringUtil.str2int(request
								.getParameter("number" + i));
						double unitprice = StringUtil.str2double(request
								.getParameter("unitprice" + i));
						double sumprice = StringUtil.str2double(request
								.getParameter("sumprice" + i));

						cdao.add(new Commodity(lastid, 1, pid, number,
								unitprice, sumprice));
					}
					response.sendRedirect("import/result.jsp?res=1&from=0&id="+ lastid);
				} else {
					response.sendRedirect("import/result.jsp?res=0&from=0");
				}

			}
		} catch (Exception e) {
			//res=1表示成功，from=0表示从添加回到result.jsp页面，与修改时回到result.jsp区分
			response.sendRedirect("import/result.jsp?res=0&from=0");
		}
	}

}
