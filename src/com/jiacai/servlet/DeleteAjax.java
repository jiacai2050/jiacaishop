package com.jiacai.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jiacai.dao.BillDAO;
import com.jiacai.dao.GuestDAO;
import com.jiacai.dao.ImportDAO;
import com.jiacai.dao.ProductDAO;
import com.jiacai.util.StringUtil;
import com.jiacai.util.Table;

@WebServlet("/DeleteAjax")
public class DeleteAjax extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int id = StringUtil.str2int(request.getParameter("id"));
		String from = request.getParameter("from");

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		boolean res = false;

		switch (Table.valueOf(from.toUpperCase())) {
		case PRODUCT:
			ProductDAO pdao = new ProductDAO();
			res = pdao.deleteById(id);
			break;
		case GUEST:
			GuestDAO gdao = new GuestDAO();
			res = gdao.deleteById(id);
			break;
		case IMPORT:
			ImportDAO idao = new ImportDAO();
			res = idao.deleteById(id);
			break;
		case BILL:
			BillDAO bdao = new BillDAO();
			res = bdao.deleteById(id);
			break;	
		default:
			break;
		}

		if (res) {
			out.print(1);
		} else {
			out.print(0);
		}
		out.flush();
		out.close();
	}

}
