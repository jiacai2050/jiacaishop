package com.jiacai.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jiacai.bean.Product;
import com.jiacai.dao.WarehouseDAO;
import com.jiacai.pagination.ProductPage;
import com.jiacai.service.ProductService;

@WebServlet("/SearchProduct")
public class SearchProduct extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		ProductService service = new ProductService();

		ProductPage page = service.genPagination(request, 3);

		List<Product>prods = page.getProducts();
		List<Long>numbers = new ArrayList<>();
		WarehouseDAO dao = new WarehouseDAO();
		for(Product p : prods) {
			numbers.add(dao.select(p.getId()).getNumber());
		}
		request.setAttribute("result", page);
		request.setAttribute("number", numbers);
		request.getRequestDispatcher("product/search.jsp").forward(request, response);

		// response.sendRedirect("result.jsp?sql="
		// + URLEncoder.encode(sql, "UTF-8"));
	}
}
