package com.jiacai.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jiacai.bean.Commodity;
import com.jiacai.bean.Import;
import com.jiacai.bean.Product;
import com.jiacai.dao.CommodityDAO;
import com.jiacai.dao.ImportDAO;
import com.jiacai.util.StringUtil;

@WebServlet("/UpdateImport")
public class UpdateImport extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		ImportDAO dao = new ImportDAO();
		CommodityDAO cdao = new CommodityDAO();

		int uid = StringUtil.str2int(request.getSession().getAttribute("uid")
				.toString());
		int id = StringUtil.str2int(request.getParameter("id"));
		Date date = StringUtil.str2date(request.getParameter("date"));
		String supplier = request.getParameter("supplier");
		int pnumber = StringUtil.str2int(request.getParameter("pnumber"));
		double sum = StringUtil.str2double(request.getParameter("sum"));

		dao.update(new Import(id, uid, date, supplier, sum));

		String delids = request.getParameter("delids");
		
		if (!StringUtil.nullOrEmpty(delids)) {
			String[] ids = delids.split(",");
			for (String toDelId : ids) {
				cdao.deleteById(StringUtil.str2int(toDelId));
			}
		}
		for (int i = 1; i <= pnumber; i++) {
			int pid = StringUtil.str2int(request.getParameter("pid" + i));
			int number = StringUtil.str2int(request.getParameter("number" + i));
			double unitprice = StringUtil.str2double(request
					.getParameter("unitprice" + i));
			double sumprice = StringUtil.str2double(request
					.getParameter("sumprice" + i));

			String cid = request.getParameter("cid" + i);//根据当前commodity有无cid确定是新增商品还是修改商品
			if (StringUtil.nullOrEmpty(cid)) {
				cdao.add(new Commodity(id, 1, pid, number, unitprice, sumprice));
			} else {
				cdao.update(new Commodity(StringUtil.str2int(cid), id, 1, pid,
						number, unitprice, sumprice));
			}
		}
		//res=1表示成功，from=1表示从修改回到result.jsp页面，与添加时回到result.jsp区分
		response.sendRedirect("import/result.jsp?res=1&from=1");	
				
	}

}
