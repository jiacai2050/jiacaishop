package com.jiacai.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jiacai.bean.Product;
import com.jiacai.dao.ProductDAO;
import com.jiacai.util.DBHelper;

@WebServlet("/AddProduct")
public class AddProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		ProductDAO dao = new ProductDAO();
		String pnumber = request.getParameter("pnumber");
		int total = Integer.valueOf(pnumber);
		if (total > 0) {
			try {
				dao.setTransactionOn();
				for (int i = 1; i <= total; i++) {
					String name = request.getParameter("name" + i);
					String specification = request.getParameter("specification"
							+ i);
					dao.add(new Product(name, specification));
				}
				dao.submitTransaction();
				response.sendRedirect("product/result.jsp?res=1&from=0");
			} catch (Exception e) {
				e.printStackTrace();
				dao.rollbackTransaction();
				response.sendRedirect("product/result.jsp?res=0&from=0");
			}
		} else {
			//res=1表示成功，from=0表示从添加回到result.jsp页面，与修改时回到result.jsp区分
			response.sendRedirect("product/result.jsp?res=0&from=0");
		}
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request, response);
	}

}
