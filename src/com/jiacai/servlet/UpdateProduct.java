package com.jiacai.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jiacai.bean.Product;
import com.jiacai.dao.ProductDAO;
import com.jiacai.util.StringUtil;

@WebServlet("/UpdateProduct")
public class UpdateProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		int id = StringUtil.str2int(request.getParameter("id"));
		String name = request.getParameter("name");
		String specification = request.getParameter("specification");
		
		ProductDAO dao = new ProductDAO();
		//res=1表示成功，from=1表示从修改回到result.jsp页面，与添加时回到result.jsp区分
		if (dao.update(new Product(id, name, specification))) {
			response.sendRedirect("product/result.jsp?res=1&from=1");	
		} else {
			response.sendRedirect("product/result.jsp?res=0&from=1");
		}
	
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request, response);
	}

}
