package com.jiacai.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.jiacai.bean.User;
import com.jiacai.dao.UserDAO;

@WebServlet("/CheckLogin")
public class CheckLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		UserDAO dao = new UserDAO();

		String name = request.getParameter("username");
		String pwd = request.getParameter("pwd");

		HttpSession session = request.getSession();

		Object status = session.getAttribute("status");
		if (status == null || !status.toString().equals("login")) {
			User user = dao.select(name);
			if (user != null) {
				if (pwd.equals(user.getPwd())) {
					session.setMaxInactiveInterval(-1);
					session.setAttribute("status", "login");
					session.setAttribute("uid", user.getId());
					response.sendRedirect("index.jsp");
				} else {
					session.setAttribute("status", "error");
					response.sendRedirect("login.jsp");
				}
			} else {
				session.setAttribute("status", "noname");
				response.sendRedirect("login.jsp");
			}
		} else {
			session.setAttribute("status", "logout");
			response.sendRedirect("login.jsp");
		}
	}

}
