package com.jiacai.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jiacai.pagination.GuestPage;
import com.jiacai.service.GuestService;

@WebServlet("/SearchGuest")
public class SearchGuest extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		GuestService service = new GuestService();
		GuestPage page = service.genPagination(request, 3);

		request.setAttribute("result", page);
		request.getRequestDispatcher("guest/search.jsp").forward(request, response);

		// response.sendRedirect("result.jsp?sql="
		// + URLEncoder.encode(sql, "UTF-8"));
	}
}
