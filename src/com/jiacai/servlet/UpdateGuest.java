package com.jiacai.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jiacai.bean.Guest;
import com.jiacai.dao.GuestDAO;
import com.jiacai.util.StringUtil;

@WebServlet("/UpdateGuest")
public class UpdateGuest extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		int id = StringUtil.str2int(request.getParameter("id"));
		String name = request.getParameter("name");
		String address = request.getParameter("address");
		String tel = request.getParameter("tel");
		GuestDAO dao = new GuestDAO();
		//res=1表示成功，from=1表示从修改回到result.jsp页面，与添加时回到result.jsp区分
		if (dao.update(new Guest(id, name, address,tel))) {
			System.out.println(1);
			response.sendRedirect("guest/result.jsp?res=1&from=1");	
		} else {
			System.out.println(0);
			response.sendRedirect("guest/result.jsp?res=0&from=1");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request, response);
	}

}

