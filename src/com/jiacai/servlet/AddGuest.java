package com.jiacai.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jiacai.bean.Guest;
import com.jiacai.bean.Product;
import com.jiacai.dao.GuestDAO;
import com.jiacai.util.StringUtil;

@WebServlet("/AddGuest")
public class AddGuest extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}
 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		GuestDAO dao = new GuestDAO();
		
		String pnumber = request.getParameter("pnumber");
		int total = StringUtil.str2int(pnumber);
		if (total > 0) {
			try {
				dao.setTransactionOn();
				for (int i = 1; i <= total; i++) {
					String name = request.getParameter("name" + i);
					String address = request.getParameter("address"
							+ i);
					String tel = request.getParameter("tel" + i);
					
					dao.add(new Guest(name, address, tel));
				}
				dao.submitTransaction();
				response.sendRedirect("guest/result.jsp?res=1&from=0");
			} catch (Exception e) {
				e.printStackTrace();
				dao.rollbackTransaction();
				response.sendRedirect("guest/result.jsp?res=0&from=0");
			}
		} else {
			//res=1表示成功，from=0表示从添加回到result.jsp页面，与修改时回到result.jsp区分
			response.sendRedirect("guest/result.jsp?res=0&from=0");
		}
	}

}
