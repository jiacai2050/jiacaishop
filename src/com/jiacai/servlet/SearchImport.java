package com.jiacai.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jiacai.bean.Import;
import com.jiacai.bean.Product;
import com.jiacai.dao.WarehouseDAO;
import com.jiacai.pagination.ImportPage;
import com.jiacai.service.ImportService;

@WebServlet("/SearchImport")
public class SearchImport extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		ImportService service = new ImportService();

		ImportPage page = service.genPagination(request, 3);

		request.setAttribute("result", page);
		
		request.getRequestDispatcher("import/search.jsp").forward(request, response);

		// response.sendRedirect("result.jsp?sql="
		// + URLEncoder.encode(sql, "UTF-8"));
	}
}
