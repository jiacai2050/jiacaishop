package com.jiacai.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

public class DBHelper {

	private static Connection conn = null;

	public static Connection getConnection() {
		ResourceBundle rb = ResourceBundle.getBundle("db");
		String driver = rb.getString("driver");
		String url = rb.getString("url");
		String username = rb.getString("username");
		String password = rb.getString("password");

		try {
			if (conn == null) {

				Class.forName(driver);
				conn = (Connection) DriverManager.getConnection(url, username,
						password);
			}
			return conn;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void main(String[] args) {
		Connection conn = getConnection();
		try {
			Statement stat = conn.createStatement();
			ResultSet rs = stat.executeQuery("select * from " + Table.WAYBILL
					+ " where 1=1 and phone like \'%8821279%\'");
			while (rs.next()) {
				System.out.println(rs.getString(2));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}
}
