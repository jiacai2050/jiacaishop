package com.jiacai.util;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;

import jxl.Workbook;
import jxl.write.DateFormat;
import jxl.write.DateTime;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.NumberFormats;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.jiacai.bean.Waybill;

public class ExcelHelper {

	public static void write(List<Waybill> waybills, OutputStream os) {
		try {

			WritableWorkbook workbook = Workbook.createWorkbook(os);
			WritableSheet sheet = workbook.createSheet("家财电器售货清单", 0);
			WritableFont arial13font = new WritableFont(WritableFont.ARIAL, 13);
			WritableCellFormat arial13format = new WritableCellFormat(
					arial13font);
			WritableCellFormat integerFormat = new WritableCellFormat(
					arial13font, NumberFormats.INTEGER);
			WritableCellFormat floatFormat = new WritableCellFormat (arial13font, NumberFormats.FLOAT); 
			WritableCellFormat dateFormat = new WritableCellFormat (arial13font, new DateFormat ("yyyy-MM-dd"));//dd MMM yyyy hh:mm:ss
			
			sheet.addCell(new Label(0, 0, "序号", arial13format)); // col row
			sheet.addCell(new Label(1, 0, "订货人", arial13format));
			sheet.addCell(new Label(2, 0, "电话", arial13format));
			sheet.addCell(new Label(3, 0, "地址", arial13format));
			sheet.addCell(new Label(4, 0, "金额", arial13format));
			sheet.addCell(new Label(5, 0, "日期", arial13format));

			int row = 1;
			for (int i = 0; i < waybills.size(); i++) {
				Waybill waybill = waybills.get(i);
				int col = 0;
				sheet.addCell(new Number(col++, row, (i + 1), integerFormat));
				sheet.addCell(new Label(col++, row, waybill.getOrdername(), arial13format));
				sheet.addCell(new Label(col++, row, waybill.getPhone(), arial13format));
				sheet.addCell(new Label(col++, row, waybill.getAddress(), arial13format));
				sheet.addCell(new Number(col++, row, waybill.getSum(), floatFormat));
				sheet.addCell(new DateTime(col++, row++, waybill.getPdate(), dateFormat));
			}
		 
			workbook.write();
			workbook.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		}
	}
}
