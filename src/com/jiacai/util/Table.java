package com.jiacai.util;

public enum Table {

	WAYBILL, PRODUCTS, PRODUCT, BILL, GUEST, IMPORT, PRICE, USER, WAREHOUSE, COMMODITY, GUESTBILL;

	public String toString() {
		return super.toString().toLowerCase();
	};
}
