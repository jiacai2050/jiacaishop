package com.jiacai.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class StringUtil {

	public static String querySQL;

	public static String null2Str(String in) {
		if (null == in) {
			return "";
		} else {
			return in.trim();
		}
	}

	public static boolean nullOrEmpty(String in) {
		return "".equals(null2Str(in));
	}

	public static Date str2date(String date) {
		String[] s = date.split("-");
		Calendar c = Calendar.getInstance();
		c.set(Integer.valueOf(s[0]), Integer.valueOf(s[1]) - 1,
				Integer.valueOf(s[2]));
		return c.getTime();
	}

	public static String date2str(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(date);
	}

	public static int str2int(String str) {
		if (str == null || "".equals(str)) {
			return 0;
		}
		return Integer.valueOf(str);
	}

	public static double str2double(String str) {
		if (str == null || "".equals(str)) {
			return 0;
		}
		return Double.valueOf(str);
	}

	public static String urlEncode(String sql) {
		try {
			return URLEncoder.encode(sql, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return "";
	}

	public static String[] genPageNavi(String baseurl, int totalRow,
			int pageNumber, int pageNo, String urlParam) {

		StringBuilder sb = new StringBuilder();
		int sumPage = totalRow / pageNumber;
		if (totalRow % pageNumber != 0) {
			sumPage= totalRow / pageNumber + 1;
		}
		if (pageNo != 1) {
			sb.append("<a href=\"" + baseurl + "?pageNo=" + (pageNo - 1)
					+ urlParam + "\">��һҳ</a>");
		}

		int begin = 0;
		if (sumPage> 10) {
			int other = sumPage- 9;
			begin = pageNo < other ? pageNo : other;
		} else {
			begin = 1;
		}
		int idx = 0;
		for (int i = begin; i <= sumPage; i++) {
			if (idx++ == 10) {
				break;
			}
			if (i == pageNo) {
				sb.append("<a class='current'>" + i + "</a>");
			} else {
				sb.append("<a  href=\"" + baseurl + "?pageNo=" + i + urlParam
						+ "\">" + i + "</a>");
			}
		}
		if (pageNo != sumPage) {
			sb.append("<a href=\"" + baseurl + "?pageNo=" + (pageNo + 1)
					+ urlParam + "\">��һҳ</a>");
		}
		return new String[] { sb.toString(), String.valueOf(sumPage) };
	}

}
