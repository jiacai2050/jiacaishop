package com.jiacai.util.test;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

import com.jiacai.util.StringUtil;

public class StringUtilTest {

	@Test
	public void testStr2Date() {
		Date d = StringUtil.str2date("2013-10-10");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Assert.assertEquals("2013-10-10", sdf.format(d));
	}
}
