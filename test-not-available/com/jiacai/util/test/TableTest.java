package com.jiacai.util.test;

import org.junit.Assert;
import org.junit.Test;

import com.jiacai.util.Table;

public class TableTest {

	@Test
	public void testToString() {
		Assert.assertEquals("waybill", Table.WAYBILL.toString());
		Assert.assertEquals(Table.PRODUCT,Table.valueOf("product".toUpperCase()));
	}
}
