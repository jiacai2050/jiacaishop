package com.jiacai.dao.test;


import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.jiacai.bean.Product;
import com.jiacai.dao.ProductDAO;
import com.jiacai.util.DBHelper;
import com.jiacai.util.Table;

public class ProductDAOTest {

	ProductDAO dao = new ProductDAO();

	@Test
	public void testselectById() throws Exception {
		Product prod = dao.select(1);
		Assert.assertEquals("金喜龙", prod.getName());
		Assert.assertEquals("40V", prod.getSpecification());
	}
	@Test
	public void testselectByName() throws Exception {
		List<Product> prods = dao.select("龙");
		Product prod = prods.get(0);
		Assert.assertEquals("金喜龙", prod.getName());
		Assert.assertEquals("40V", prod.getSpecification());
	}
	@Test
	public void testAdd() throws Exception {
		
		Product prod = new Product("小米", "3S");
		Assert.assertTrue(dao.add(prod));
		prod = dao.select(dao.getLastInsertID(Table.PRODUCT));
		
		Assert.assertEquals("小米", prod.getName());
		Assert.assertEquals("3S", prod.getSpecification());
		
	}
	@Test
	public void testDeleteByIDs() {
		Assert.assertEquals(dao.deleteByIds(new int[]{3 ,4}),2);
	}
	
}
