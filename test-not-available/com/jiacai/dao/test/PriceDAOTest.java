package com.jiacai.dao.test;

import junit.framework.Assert;

import org.junit.Test;

import com.jiacai.bean.Price;
import com.jiacai.dao.PriceDAO;

public class PriceDAOTest {

	PriceDAO dao = new PriceDAO();
	
	@Test
	public void testUpdate() {
		Price price = new Price(4,1,10);
		dao.update(price);
		
		price = new Price(4,3,10);
		dao.update(price);
	}
	@Test
	public void testSelect() {
		Price p = dao.select(4, 1);
		Assert.assertEquals(10.0, p.getUnitprice());
		Assert.assertNull(dao.select(4,40));
	}
}
