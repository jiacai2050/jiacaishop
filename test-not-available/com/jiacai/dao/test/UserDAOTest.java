package com.jiacai.dao.test;


import org.junit.Assert;
import org.junit.Test;

import com.jiacai.bean.User;
import com.jiacai.dao.UserDAO;

public class UserDAOTest {

	UserDAO dao = new UserDAO();
	
	@Test
	public void testAdd() {
		User user = new User(1, "ljc", "0219");
		Assert.assertTrue(dao.add(user));
		
	}
	@Test
	public void testSelect() {
		User user = dao.select("admin");
		Assert.assertNotNull(user);
		Assert.assertEquals("admin", user.getName());
		Assert.assertEquals(1, user.getRole());
		Assert.assertEquals("101010", user.getPwd());
		
	}
}
