package com.jiacai.dao.test;

import org.junit.Assert;
import org.junit.Test;

import com.jiacai.bean.Guest;
import com.jiacai.dao.GuestDAO;
import com.jiacai.util.Table;

public class GuestDAOTest {

	GuestDAO dao = new GuestDAO();
	
	@Test
	public void testUpdate() {
		Guest guest = new Guest(4, "hehe", "ly", "hd");
		Assert.assertTrue(dao.update(guest));
		guest = dao.select(dao.getLastInsertID(Table.GUEST));
		Assert.assertEquals("hehe", guest.getName());
		Assert.assertEquals("ly", guest.getAddress());
		Assert.assertEquals("hd", guest.getTel());
	}
}
